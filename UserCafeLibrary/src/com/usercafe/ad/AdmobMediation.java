package com.usercafe.ad;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.customevent.CustomEventBanner;
import com.google.ads.mediation.customevent.CustomEventBannerListener;

public class AdmobMediation implements CustomEventBanner{
	UserCafeADView mAdView;
	@Override
	public void destroy() {
		
	}

	@Override
	public void requestBannerAd(final CustomEventBannerListener listener, Activity arg1,
			String label, String serverParameter, AdSize adSize, MediationAdRequest request,
			Object customEventExtra) {
		if(mAdView == null){
			mAdView = new UserCafeADView(arg1);
		}
		mAdView.mDevId = serverParameter;
		mAdView.reload(new ReturnCallback() {
			
			@Override
			public void onSuccess(View view) {
				listener.onReceivedAd(view);
			}
			
			@Override
			public void onFailed() {
				Log.w("UserCafeAD","No AD");
				listener.onFailedToReceiveAd();				
			}

			@Override
			public void onFinish() {
				listener.onDismissScreen();
			}

			@Override
			public void onStart() {
				listener.onPresentScreen();
				
			}
		});
	}
}
