package com.usercafe.ad;
import android.view.View;

public interface ReturnCallback {
	public void onSuccess(View view);

	public void onFailed();

	public void onStart();

	public void onFinish();
}