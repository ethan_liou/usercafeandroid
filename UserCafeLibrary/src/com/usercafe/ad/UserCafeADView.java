package com.usercafe.ad;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.usercafe.core.Constant;
import com.usercafe.core.PredefinedCustomData;
import com.usercafe.core.UserCafe;
import com.usercafe.core.UserCafe.CacheMode;
import com.usercafe.utils.Base64;
import com.usercafe.utils.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class UserCafeADView extends RelativeLayout implements
		View.OnClickListener {
	final String TAG = "UserCafeAD";
	boolean mAutoReload;
	Context mContext;
	String mLocationStr;
	ReturnCallback mCallback;
	JSONObject mCurrentAD;
	String mDevId;
	final String KEY_DEV_ID = "_developer_id";
	final String KEY_DEVICE_MODEL = "_device_model";
	final String KEY_LOCATION = "_location";
	final String KEY_BANNER_IDX = "_banner_idx";
	final String AD_APP_ID = "520195602a4f5641c87921ed";
	
	public UserCafeADView(Context context) {
		this(context, null);
	}

	public UserCafeADView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public void setDevId(String devId){
		mDevId = devId;
	}
	
	public UserCafeADView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		mLocationStr = null;
		mAutoReload = true;
		setBackgroundColor(Color.BLACK);
	}

	public void GetLocation() {
		try {
			final LocationManager lm = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);
			List<String> providers = lm.getProviders(true);
			for (String provider : providers) {
				lm.requestLocationUpdates(provider, 0, 0,
						new LocationListener() {
							@Override
							public void onStatusChanged(String provider,
									int status, Bundle extras) {
							}

							@Override
							public void onProviderEnabled(String provider) {
							}

							@Override
							public void onProviderDisabled(String provider) {
							}

							@Override
							public void onLocationChanged(Location location) {
								if (mLocationStr == null) {
									mLocationStr = "" + location.getLatitude()
											+ "," + location.getLongitude();
								}
								lm.removeUpdates(this);
							}
						});
			}
		} catch (Exception e) {

		}
	}

	
	
	public boolean GetLastLocation() {
		try {
			LocationManager lm = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);
			List<String> providers = lm.getProviders(true);
			for (String provider : providers) {
				Location location = lm.getLastKnownLocation(provider);
				if (location != null) {
					mLocationStr = "" + location.getLatitude() + ","
							+ location.getLongitude();
					return true;
				}
			}
		} catch (Exception e) {
			Toast.makeText(mContext, "Error " + e, Toast.LENGTH_LONG).show();
		}
		return false;
	}

	public void reload(final ReturnCallback callback) {
		if(!UserCafe.sharedInstance().isInit()){
			Log.e(TAG,"You should call UserCafe.AppCreate(appId) first");
		}
		mCallback = callback;
		removeAllViews();
		if (mLocationStr == null) {
			GetLocation();
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				JSONObject ad = null;
				try {
					HttpClient client = Utils.generateClient();
					HttpGet request = new HttpGet(Constant.SERVER_URL+"/adapi/ads/user/"+PredefinedCustomData.deviceId(mContext));
					HttpResponse response = client.execute(request);
					if (response.getStatusLine().getStatusCode() == 200) {
						String retSrc = EntityUtils.toString(response
								.getEntity());
						ad = new JSONObject(retSrc);
					} else if (response.getStatusLine().getStatusCode() == 204
							&& callback != null) {
						// no data
						callback.onFailed();
						return;
					}
				} catch (Exception e) {
					// ignore...
					if (callback != null)
						callback.onFailed();
					return;
				}
				mCurrentAD = ad;
				DisplayMetrics metrics = new DisplayMetrics();
				((Activity) mContext).getWindowManager()
						.getDefaultDisplay()
						.getMetrics(metrics);
				final float density = metrics.density;
				final int height = (int) (48 * density);
				if (mCurrentAD != null) {
					((Activity) mContext).runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mCurrentAD.has("banner")) {
								String uri = mCurrentAD.optString("banner");
								String base64ed_str = uri.split(",")[1];
								byte[] decodedString = Base64
										.decode(base64ed_str);
								Bitmap bm = BitmapFactory.decodeByteArray(
										decodedString, 0, decodedString.length);

								int bmWidth = bm.getWidth();
								int bmHeight = bm.getHeight();

								ImageView btn = new ImageView(mContext);
								btn.setScaleType(ScaleType.FIT_XY);
								DisplayMetrics metrics = new DisplayMetrics();
								((Activity) mContext).getWindowManager()
										.getDefaultDisplay()
										.getMetrics(metrics);
								final float ratio = 1.0f * height
										/ bmHeight;
								RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
										(int) (bmWidth * ratio),
										(int) (bmHeight * ratio));
								params.addRule(RelativeLayout.CENTER_IN_PARENT);
								btn.setLayoutParams(params);
								btn.setOnClickListener(UserCafeADView.this);

								btn.setImageBitmap(bm);
								btn.setClickable(true);
								removeAllViews();
								addView(btn);
								if (callback != null) {
									callback.onSuccess(UserCafeADView.this);
								}
							} else {
								Button btn = new Button(mContext);
								btn.setBackgroundColor(Color.TRANSPARENT);
								btn.setTextColor(Color.WHITE);
								btn.setTextSize(18);

								btn.setLayoutParams(new ViewGroup.LayoutParams(
										ViewGroup.LayoutParams.MATCH_PARENT,height));
								btn.setText(mCurrentAD.optString("title"));
								btn.setOnClickListener(UserCafeADView.this);
								removeAllViews();
								addView(btn);
								if (callback != null) {
									callback.onSuccess(UserCafeADView.this);
								}
							}
						}
					});
				}
			}
		}).start();
	}

	@Override
	public void onClick(View v) {
		HashMap<String, String> data = new HashMap<String, String>();
		for(Method method : PredefinedCustomData.class.getDeclaredMethods()){
			try {
				if(method.getName().equals("callFuncByName"))
					continue;
				String value = (String)method.invoke(null, mContext);
				data.put("_"+method.getName(),value);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		if(mDevId != null)
			data.put(KEY_DEV_ID, mDevId);
		if (mLocationStr != null || GetLastLocation()) {
			data.put(KEY_LOCATION, mLocationStr);
		}
		int bannerIdx = mCurrentAD.optInt("banner_idx", -1);
		if(bannerIdx >= 0)
			data.put(KEY_BANNER_IDX, ""+bannerIdx);
		Intent intent = UserCafe.getStaticIntent(mContext, AD_APP_ID, mCurrentAD.optString("survey_id"), null, CacheMode.NO_CACHE, data,null);
		getContext().startActivity(intent);
		if (mCallback != null)
			mCallback.onStart();
	}
}
