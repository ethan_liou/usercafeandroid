package com.usercafe.core;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

class AnswerCache {
	private Map<String, String> cacheMap;
	private String mSurveyId;

	AnswerCache(String surveyId) {
		super();
		mSurveyId = surveyId;
		cacheMap = new HashMap<String, String>();
	}

	void loadAllCacheToMap(Context context) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getReadableDatabase();
			Cursor cursor = db.rawQuery(
					"SELECT _key,_value FROM AnswerCache WHERE _survey_id=?",
					new String[] { mSurveyId });
			while (cursor.moveToNext()) {
				cacheMap.put(cursor.getString(0), cursor.getString(1));
			}
			db.close();
		} catch (Exception e) {
			// pass
		}
	}

	void clearCache(Context context) {
		cacheMap.clear();
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			db.delete("AnswerCache", "_survey_id=?", new String[] { mSurveyId });
			db.close();
		} catch (Exception e) {
			// pass
		}
	}

	void saveCache(BaseQuestion question, String answer) {
		if (answer == null && cacheMap.containsKey(question.mName)) {
			cacheMap.remove(question.mName);
		} else {
			cacheMap.put(question.mName, answer);
		}
	}

	void commit(Context context) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			for (String key : cacheMap.keySet()) {
				ContentValues values = new ContentValues(3);
				values.put("_survey_id", mSurveyId);
				values.put("_key", key);
				values.put("_value", cacheMap.get(key));
				db.replace("AnswerCache", null, values);
			}
			db.close();
		} catch (Exception e) {
			// pass
		}
	}

	String loadCache(BaseQuestion question) {
		return cacheMap.get(question.mName);
	}
}
