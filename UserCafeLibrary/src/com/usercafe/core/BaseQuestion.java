package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

abstract class BaseQuestion {
	String TAG = "BaseQuestion";
	WeakReference<SurveyContext> mSurveyContext;
	
	long mStartTime,mStopTime;
	
	Context mContext;
	String mTitle;
	String mDesc;
	boolean mRequired;
	int mType;
	String mName;
	boolean mLastQuestion;
	boolean mFirstQuestion;
	PageManager mOwner;
	int mPos;
	int mRealPos;
	String mAnswer;
	BaseQuestionView mBaseView;
	
	// layer
	String mFirstLayer;
	String mSecondLayer;
	String mThirdLayer;
	
	// other
	String mOtherStr;
	boolean mOther;

	BaseQuestion(Context context,WeakReference<SurveyContext> surveyContext){
		mTitle = null;
		mDesc = null;
		mRequired = false;
		mType = -1;
		mName = null;
		mAnswer = null;	
		mLastQuestion = false;
		mFirstQuestion = false;
		mPos = -1;
		mRealPos = -1;
		mFirstLayer = null;
		mSecondLayer = null;
		mThirdLayer = null;
		mContext = context;
		mSurveyContext = surveyContext;
	}
	
	BaseQuestion(Context context,WeakReference<SurveyContext> surveyContext,JSONObject json_obj){
		this(context,surveyContext);
		if(json_obj != null){
			mType = json_obj.optInt("type", -1);
			mTitle = json_obj.optString("title", "");
			mDesc = json_obj.optString("desc", "");
			mRequired = json_obj.optInt("required", 0) == 1;
			mName = json_obj.optString("name", "");
		}
		loadAnswer();
	}
	View renderView(){
		mBaseView = new BaseQuestionView(mContext,mSurveyContext);
		mBaseView.mRequiredView.setVisibility(mRequired ? View.VISIBLE : View.GONE);
		mBaseView.mTitleView.setText(mTitle == null ? "" : mTitle);
		mBaseView.mDescView.setText(mDesc == null ? "" : mDesc);
		View contentV = fillContentView(mBaseView.mContentLayout);
		if(contentV == null){
			return null;
		}
		loadAnswer();
		if(!fillAnswer(mBaseView.mContentLayout,mAnswer)){
			return null;
		}
		modifyTitleAndDesc(mBaseView.mTitleView, mBaseView.mDescView);
		return mBaseView;
	}
	
	String getAnswer(){
		return mAnswer;
	}
	
	void loadAnswer(){
		mAnswer = mSurveyContext.get().mAnswerCache.loadCache(this);
	}
		
	void setAnswer(String answer){
		mAnswer = answer;
		if(answer != null)
			mSurveyContext.get().mAnswerCache.saveCache(this,answer);
	}
	
	void modifyTitleAndDesc(TextView titleTV,TextView descTV){
		// do nothing
	}
	abstract View fillContentView(ViewGroup container);
	abstract boolean fillAnswer(ViewGroup container,String answer);
}
