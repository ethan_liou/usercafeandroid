package com.usercafe.core;

import java.lang.ref.WeakReference;

import com.usercafe.utils.Utils;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

class BaseQuestionView extends ScrollView {
	// id
	private static final int OFFSET_ID = 1000;
	static final int TITLE_ID = OFFSET_ID + 1;
	static final int DESC_ID = OFFSET_ID + 2;
	static final int CONTENT_ID = OFFSET_ID + 3;

	private LinearLayout baseLayout;
	TextView mTitleView;
	TextView mDescView;
	TextView mRequiredView;
	LinearLayout mContentLayout;
	WeakReference<SurveyContext> mSurveyContext;

	public BaseQuestionView(Context context){
		super(context);
		throw(new UnsupportedOperationException());
	}
	
	public BaseQuestionView(Context context,WeakReference<SurveyContext> surveyContext) {
		super(context);
		mSurveyContext = surveyContext;
		setScrollbarFadingEnabled(false);
		// setup baselayout
		{
			baseLayout = new LinearLayout(context);
			baseLayout.setOrientation(LinearLayout.VERTICAL);
			baseLayout.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			int padding = Utils.getPxFromDip(context, 20);
			baseLayout.setPadding(padding, Utils.getPxFromDip(context, 15), padding, padding);
		}
		// setup title
		{
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			LinearLayout horiLL = new LinearLayout(context);
			horiLL.setOrientation(LinearLayout.HORIZONTAL);
			horiLL.setLayoutParams(params);
			mRequiredView = new TextView(context);
			mRequiredView.setTextColor(Color.RED);
			mRequiredView.setText("��");
			mRequiredView.setTextSize(18);
			horiLL.addView(mRequiredView);
			
			mTitleView = new TextView(context);
			mTitleView.setId(TITLE_ID);
			mTitleView.setTextColor(mSurveyContext.get().getColorObject().text);
			mTitleView.setTextSize(18);
			horiLL.addView(mTitleView);
			baseLayout.addView(horiLL);
		}
		// setup desc
		{
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			params.topMargin = Utils.getPxFromDip(context, 5);
			mDescView = new TextView(context);
			mDescView.setId(DESC_ID);
			mDescView.setLayoutParams(params);
			mDescView.setTextColor(mSurveyContext.get().getColorObject().text);
			mDescView.setTextSize(16);

			baseLayout.addView(mDescView);
		}
		// setup content
		{
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			params.topMargin = Utils.getPxFromDip(context, 5);
			mContentLayout = new LinearLayout(context);
			mContentLayout.setOrientation(LinearLayout.VERTICAL);
			mContentLayout.setId(CONTENT_ID);
			mContentLayout.setLayoutParams(params);
			baseLayout.addView(mContentLayout);
		}
		addView(baseLayout);
	}
}
