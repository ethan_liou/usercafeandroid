package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.CustomCheckBox;
import com.usercafe.ui.UnscrollableListView;
import com.usercafe.utils.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;

class CheckBoxQuestion extends BaseQuestion implements
		UnscrollableListView.OnItemClickListener {
	private static final int OFFSET = 3000;
	private static final int CHECK_BOX_ID = OFFSET + 1;

	ArrayList<String> mOptions;
	ArrayList<Boolean> mChecked;
	UnscrollableListView mLV;
	String TAG_PREFIX = "CB_";

	// other
	int mOtherIndex;
	boolean mDontJumpDialog;
	TextView mOtherTV;

	CheckBoxQuestion(Context context, WeakReference<SurveyContext> surveyContext, JSONObject json_obj) throws JSONException {
		super(context, surveyContext, json_obj);
		JSONArray optionArr = json_obj.getJSONArray("options");
		mOptions = new ArrayList<String>(optionArr.length());
		mChecked = new ArrayList<Boolean>(optionArr.length());
		mOtherIndex = -1;
		for (int i = 0; i < optionArr.length(); i++) {
			mOptions.add(optionArr.getString(i));
			mChecked.add(false);
		}
	}

	private void openOtherDialog(final Context context) {
		final EditText otherET = new EditText(context);
		if (mOtherStr != null) {
			otherET.setText(mOtherStr);
		}
		new AlertDialog.Builder(context)
				.setTitle(UserCafeString.getString(UserCafeString.other_hint))
				.setView(otherET)
				.setNegativeButton(
						UserCafeString.getString(UserCafeString.cancel),
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						})
				.setPositiveButton(UserCafeString.getString(UserCafeString.ok),
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								mOtherStr = otherET.getText().toString();
								mOtherTV.setText(String.format(
										"%s : %s",
										UserCafeString
												.getString(UserCafeString.other_hint),
										mOtherStr));
							}
						}).show();
	}

	class CheckboxAdapter extends BaseAdapter {
		Context mContext;

		public CheckboxAdapter(Context context) {
			mContext = context;
		}

		@Override
		public int getCount() {
			return mOptions.size();
		}

		@Override
		public Object getItem(int position) {
			return mOptions.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			RelativeLayout rl = new RelativeLayout(mContext);
			rl.setLayoutParams(new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT));
			int padding = Utils.getPxFromDip(mContext, 14);
			rl.setPadding(padding, padding, padding, padding);

			// cb
			CustomCheckBox cb = new CustomCheckBox(mContext, mSurveyContext.get().getColorObject().icon);
			int cbSize = Utils.getPxFromDip(mContext, 26);
			RelativeLayout.LayoutParams cbParams = new RelativeLayout.LayoutParams(
					cbSize, cbSize);
			cbParams.addRule(RelativeLayout.CENTER_VERTICAL);
			cbParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			cb.setLayoutParams(cbParams);
			cb.setId(CHECK_BOX_ID);
			cb.setTag(TAG_PREFIX + position);
			cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					String tag = (String) buttonView.getTag();
					if (tag.equals(String.format("%s%d", TAG_PREFIX,
							mOtherIndex))) {
						mOther = isChecked;
						if (mOther) {
							if (mDontJumpDialog) {
								mDontJumpDialog = false;
							} else {
								openOtherDialog(mContext);
							}
						}
					}

					mChecked.set(position, isChecked);
					String answer = "";
					for (int i = 0; i < mChecked.size(); i++) {
						Boolean checked = mChecked.get(i);
						if (!checked)
							continue;
						answer = answer.concat(mOptions.get(i) + "\n");
					}
					if (answer == "")
						answer = null;
					else {
						answer = answer.substring(0, answer.length() - 1);
					}
					setAnswer(answer);
				}
			});
			rl.addView(cb);

			TextView textTV = new TextView(mContext);
			RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			textParams.addRule(RelativeLayout.CENTER_VERTICAL);
			textParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			textParams.addRule(RelativeLayout.LEFT_OF, CHECK_BOX_ID);
			textTV.setLayoutParams(textParams);
			textTV.setTextColor(mSurveyContext.get().getColorObject().button_text);
			textTV.setTextSize(16);

			// replace other
			String opt = (String) getItem(position);
			if (opt.equalsIgnoreCase(Constant.getOtherKey(mSurveyContext.get().getVersion()))) {
				mOtherTV = textTV;
				mOtherIndex = position;
				String otherStr = mOtherStr == null ? "" : " : " + mOtherStr;
				opt = String.format("%s%s",UserCafeString.getString(UserCafeString.other_hint), otherStr);
			}
			textTV.setText(opt);
			rl.addView(textTV);

			return rl;
		}
	}

	@Override
	View fillContentView(ViewGroup container) {
		mLV = new UnscrollableListView(mContext, mSurveyContext.get().getColorObject().button_bg);
		mLV.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		mLV.setOnItemClickListener(this);
		mLV.setAdapter(new CheckboxAdapter(mContext));
		container.addView(mLV);
		return mLV;
	}

	int contentIndexOf(String str) {
		for (String opt : mOptions) {
			if (str.equals(opt))
				return mOptions.indexOf(opt);
		}
		return -1;
	}

	@Override
	protected boolean fillAnswer(ViewGroup container, String answer) {
		if (answer == null)
			return true;
		String[] answers = answer.split("\n");
		for (String ans : answers) {
			if (ans.length() == 0)
				continue;
			int pos = mOptions.indexOf(ans);
			if (pos == -1) {
				return false;
			}
			CheckBox cb = (CheckBox) container
					.findViewWithTag(TAG_PREFIX + pos);
			if (cb != null) {
				if (pos == mOtherIndex)
					mDontJumpDialog = true;
				cb.setChecked(true);
			}
		}
		return true;
	}

	@Override
	public void onItemClick(ViewGroup parent, View view, int position, Object o) {
		CheckBox cb = (CheckBox) mLV.findViewWithTag(TAG_PREFIX + position);
		if (cb != null) {
			boolean isChecked = cb.isChecked();
			cb.setChecked(isChecked ? false : true);
		}

	}

}
