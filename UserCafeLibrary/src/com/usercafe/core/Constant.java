package com.usercafe.core;

public class Constant {
	// pref
	static String DATA_PREF = "data.pref";
	static String STATUS_KEY = "status";
	static String MD5_KEY = "md5";
	
	public static String SERVER_URL = "http://usercafe.com";

	static String getOtherKey(int version){
		switch(version){
		case 1:
			return "__option__";
		case 2:
			return "__other_option__";
		}
		return null;
	}
	
	static String getOtherParamKey(int version){
		switch(version){
		case 1:
			return "other_option_";
		case 2:
			return "other_option_response";
		}
		return null;		
	}	
}
