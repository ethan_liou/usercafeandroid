package com.usercafe.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DatabaseHelper extends SQLiteOpenHelper {
	private static final int VERSION = 4;
	private static final String DB_NAME = "usercafe.sqlite";
	private final String QUESTIONNAIRE_INIT = "CREATE TABLE 'Survey' ('_survey_id' TEXT PRIMARY KEY NOT NULL ,'_app_id' TEXT NOT NULL ,'_status' INTEGER NOT NULL  ,'_status_data' TEXT,'_time' INTEGER NOT NULL)";
	private final String QUESTIONNAIRE_DELETE = "DROP TABLE IF EXISTS 'Survey'";
	private final String ANSWERCACHE_INIT = "CREATE TABLE 'AnswerCache' ('_survey_id' TEXT NOT NULL , '_key' TEXT NOT NULL , '_value' TEXT NOT NULL , PRIMARY KEY ('_survey_id', '_key'))";
	private final String ANSWERCACHE_DELETE = "DROP TABLE IF EXISTS 'AnswerCache'";
	private final String PENDINGANSWER_INIT = "CREATE TABLE 'PendingAnswer' ('_tid' INTEGER NOT NULL , '_key' TEXT NOT NULL , '_value' TEXT NOT NULL )";
	private final String PENDINGANSWER_DELETE = "DROP TABLE IF EXISTS 'PendingAnswer'";
	private final String TRANSACTION_TABLE_INIT = "CREATE TABLE 'TransactionTable' ('_tid' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , '_app_id' TEXT NOT NULL, '_survey_id' TEXT NOT NULL, '_time' INTEGER NOT NULL)";
	private final String TRANSACTION_TABLE_DELETE = "DROP TABLE IF EXISTS 'TransactionTable'";

	private static DatabaseHelper currentContext;

	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	static DatabaseHelper sharedDBHelper(Context context) {
		if (currentContext == null) {
			currentContext = new DatabaseHelper(context);
		}
		return currentContext;
	}

	boolean createAllTables(SQLiteDatabase db) {
		// onCreate create table
		try {
			db.execSQL(QUESTIONNAIRE_INIT);
			db.execSQL(ANSWERCACHE_INIT);
			db.execSQL(PENDINGANSWER_INIT);
			db.execSQL(TRANSACTION_TABLE_INIT);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	boolean deleteAllTables(SQLiteDatabase db) {
		try {
			db.execSQL(QUESTIONNAIRE_DELETE);
			db.execSQL(ANSWERCACHE_DELETE);
			db.execSQL(PENDINGANSWER_DELETE);
			db.execSQL(TRANSACTION_TABLE_DELETE);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createAllTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("DBUpgrade",""+oldVersion+" to " + newVersion);
		if (!deleteAllTables(db) || !createAllTables(db)) {
			db.setVersion(oldVersion);
		}
	}
}
