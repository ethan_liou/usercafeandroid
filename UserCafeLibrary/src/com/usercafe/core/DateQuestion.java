package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import com.usercafe.resource.UserCafeImage;
import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.RectDrawable;
import com.usercafe.utils.Base64;
import com.usercafe.utils.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker.OnTimeChangedListener;

public class DateQuestion extends BaseQuestion implements
		OnDateChangedListener, OnTimeChangedListener, OnClickListener {
	enum DateType {
		DATE_TYPE_YEAR_MONTH_DAY, DATE_TYPE_YEAR_MONTH_DAY_TIME, DATE_TYPE_MONTH_DAY, DATE_TYPE_MONTH_DAY_TIME
	}

	String mDateString;
	String mTimeString;
	String mDateFormat;
	String mTimeFormat;
	DateType mDateType;
	final int TIME_ID = 1;
	final int DATE_ID = 2;

	DateQuestion(Context context, WeakReference<SurveyContext> surveyContext,
			JSONObject json_obj) {
		super(context, surveyContext, json_obj);
		mDateType = DateType.values()[json_obj.optInt("date_type", 0)];
	}

	DatePicker generateDatePicker() {
		// date picker
		DatePicker datePicker = new DatePicker(mContext);
		Date initDate = null;
		if (mDateString != null) {
			try {
				DateFormat formatter = new SimpleDateFormat(mDateFormat,
						Locale.getDefault());
				initDate = formatter.parse(mDateString);
			} catch (ParseException e) {
				initDate = new Date(System.currentTimeMillis());
			}
		} else {
			initDate = new Date(System.currentTimeMillis());
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(initDate);
		datePicker
				.init(calendar.get(Calendar.YEAR),
						calendar.get(Calendar.MONTH),
						calendar.get(Calendar.DATE), this);
		if (Build.VERSION.SDK_INT >= 11) {
			datePicker.setCalendarViewShown(false);
		}
		if (mDateType.equals(DateType.DATE_TYPE_MONTH_DAY)
				|| mDateType.equals(DateType.DATE_TYPE_MONTH_DAY_TIME)) {
			try {
				Field f[] = datePicker.getClass().getDeclaredFields();
				for (Field field : f) {
					if (field.getName().equals("mYearSpinner")) {
						field.setAccessible(true);
						Object yearPicker = new Object();
						yearPicker = field.get(datePicker);
						((View) yearPicker).setVisibility(View.GONE);
					}
				}
			} catch (Exception e) {
			}
		}
		return datePicker;
	}

	TimePicker generateTimePicker() {
		// time picker
		TimePicker picker = null;
		if (mDateType.equals(DateType.DATE_TYPE_MONTH_DAY_TIME)
				|| mDateType.equals(DateType.DATE_TYPE_YEAR_MONTH_DAY_TIME)) {
			picker = new TimePicker(mContext);
			Date initDate = null;
			if (mTimeString != null) {
				try {
					DateFormat formatter = new SimpleDateFormat(mTimeFormat,
							Locale.getDefault());
					initDate = formatter.parse(mTimeString);
				} catch (ParseException e) {
					initDate = new Date(System.currentTimeMillis());
				}
			} else {
				initDate = new Date(System.currentTimeMillis());
			}

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(initDate);

			picker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
			picker.setCurrentMinute(calendar.get(Calendar.MINUTE));
			picker.setOnTimeChangedListener(this);
		}
		return picker;
	}

	@Override
	View fillContentView(ViewGroup container) {
		// init date formatter
		switch (mDateType) {
		case DATE_TYPE_MONTH_DAY:
			mDateFormat = "MM/dd";
			break;
		case DATE_TYPE_YEAR_MONTH_DAY:
			mDateFormat = "yyyy/MM/dd";
			break;
		case DATE_TYPE_YEAR_MONTH_DAY_TIME:
			mDateFormat = "yyyy/MM/dd";
			mTimeFormat = "HH:mm";
			break;
		case DATE_TYPE_MONTH_DAY_TIME:
			mDateFormat = "MM/dd";
			mTimeFormat = "HH:mm";
			break;
		}

		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);

		Button dateBtn = new Button(mContext);
		Utils.setBackgroundDrawable(dateBtn, new RectDrawable(mSurveyContext
				.get().getColorObject().button_bg));
		dateBtn.setText(UserCafeString
				.getString(UserCafeString.date_placeholder));
		dateBtn.setTextColor(mSurveyContext
				.get().getColorObject().button_text);
		byte[] dateImage = Base64.decode(UserCafeImage.DATE);
		int imageSize = Utils.getPxFromDip(mContext, 40);
		dateBtn.setId(DATE_ID);
		dateBtn.setOnClickListener(this);
		dateBtn.setCompoundDrawablesWithIntrinsicBounds(
				new BitmapDrawable(mContext.getResources(), Utils.scaleBitmap(
						dateImage, imageSize, imageSize)), null, null, null);
		ll.addView(dateBtn);
		if (mDateType.equals(DateType.DATE_TYPE_MONTH_DAY_TIME)
				|| mDateType.equals(DateType.DATE_TYPE_YEAR_MONTH_DAY_TIME)) {
			Button timeBtn = new Button(mContext);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			params.topMargin = Utils.getPxFromDip(mContext, 10);
			timeBtn.setLayoutParams(params);
			Utils.setBackgroundDrawable(timeBtn, new RectDrawable(
					mSurveyContext.get().getColorObject().button_bg));
			timeBtn.setText(UserCafeString
					.getString(UserCafeString.clock_placeholder));
			timeBtn.setTextColor(mSurveyContext
					.get().getColorObject().button_text);
			timeBtn.setId(TIME_ID);
			timeBtn.setOnClickListener(this);
			byte[] clockImage = Base64.decode(UserCafeImage.CLOCK);
			timeBtn.setCompoundDrawablesWithIntrinsicBounds(
					new BitmapDrawable(mContext.getResources(), Utils
							.scaleBitmap(clockImage, imageSize, imageSize)),
					null, null, null);
			ll.addView(timeBtn);
		}
		container.addView(ll);
		return ll;
	}

	void setDateTimeAnswer() {
		switch (mDateType) {
		case DATE_TYPE_MONTH_DAY:
			if (mDateString != null)
				setAnswer(mDateString);
			break;
		case DATE_TYPE_YEAR_MONTH_DAY:
			if (mDateString != null)
				setAnswer(mDateString);
			break;
		case DATE_TYPE_YEAR_MONTH_DAY_TIME:
			if (mDateString != null && mTimeString != null)
				setAnswer(mDateString + " " + mTimeString);
			break;
		case DATE_TYPE_MONTH_DAY_TIME:
			if (mDateString != null && mTimeString != null)
				setAnswer(mDateString + " " + mTimeString);
			break;
		}
	}

	void changeAnswer(DatePicker datePicker, TimePicker timePicker) {
		switch (mDateType) {
		case DATE_TYPE_MONTH_DAY:
			if (datePicker != null)
				mDateString = String.format("%02d/%02d",
						datePicker.getMonth() + 1, datePicker.getDayOfMonth());
			break;
		case DATE_TYPE_YEAR_MONTH_DAY:
			if (datePicker != null)
				mDateString = String.format("%04d/%02d/%02d",
						datePicker.getYear(), datePicker.getMonth() + 1,
						datePicker.getDayOfMonth());
			break;
		case DATE_TYPE_YEAR_MONTH_DAY_TIME:
			if (datePicker != null)
				mDateString = String.format("%04d/%02d/%02d",
						datePicker.getYear(), datePicker.getMonth() + 1,
						datePicker.getDayOfMonth());
			if (timePicker != null)
				mTimeString = String.format("%02d:%02d",
						timePicker.getCurrentHour(),
						timePicker.getCurrentMinute());
			break;
		case DATE_TYPE_MONTH_DAY_TIME:
			if (datePicker != null)
				mDateString = String.format("%02d/%02d",
						datePicker.getMonth() + 1, datePicker.getDayOfMonth());
			if (timePicker != null)
				mTimeString = String.format("%02d:%02d",
						timePicker.getCurrentHour(),
						timePicker.getCurrentMinute());
			break;
		}
	}

	@Override
	boolean fillAnswer(ViewGroup container, String answer) {
		if (answer != null && answer.length() != 0)
			switch (mDateType) {
			case DATE_TYPE_MONTH_DAY:
			case DATE_TYPE_YEAR_MONTH_DAY: {
				mDateString = answer;
				Button btn = (Button) container.findViewById(DATE_ID);
				btn.setText(mDateString);
				;
				break;
			}
			case DATE_TYPE_YEAR_MONTH_DAY_TIME:
			case DATE_TYPE_MONTH_DAY_TIME: {
				String[] strs = answer.split(" ");
				mDateString = strs[0];
				mTimeString = strs[1];
				Button dateBtn = (Button) container.findViewById(DATE_ID);
				dateBtn.setText(mDateString);
				;
				Button timeBtn = (Button) container.findViewById(TIME_ID);
				timeBtn.setText(mTimeString);
				;
				break;
			}
			}
		return true;
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		changeAnswer(null, view);
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		changeAnswer(view, null);
	}

	@Override
	public void onClick(View arg0) {
		final Button btn = (Button) arg0;
		final int id = arg0.getId();
		final View view = id == DATE_ID ? generateDatePicker()
				: generateTimePicker();
		new AlertDialog.Builder(mContext)
				.setTitle(
						UserCafeString
								.getString(id == DATE_ID ? UserCafeString.date_placeholder
										: UserCafeString.clock_placeholder))
				.setView(view)
				.setPositiveButton(UserCafeString.getString(UserCafeString.ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if (id == DATE_ID) {
									changeAnswer((DatePicker) view, null);
									btn.setText(mDateString);
								}
								if (id == TIME_ID) {
									changeAnswer(null, (TimePicker) view);
									btn.setText(mTimeString);
								}
								setDateTimeAnswer();
							}
						})
				.setNegativeButton(
						UserCafeString.getString(UserCafeString.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {

							}
						}).show();
	}
}
