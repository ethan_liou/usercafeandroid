package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public class GridQuestion extends BaseQuestion {
	List<String> mColumnLabels;
	List<String> mRowLabels;
	List<String> mNames;
	JSONObject mInputObj;

	GridQuestion(Context context, WeakReference<SurveyContext> surveyContext,
			JSONObject json_obj) throws JSONException {
		super(context, surveyContext, json_obj);
		JSONArray columnLabels = json_obj.getJSONArray("score_labels");
		mColumnLabels = new ArrayList<String>(columnLabels.length());
		for (int i = 0; i < columnLabels.length(); i++) {
			mColumnLabels.add(columnLabels.getString(i));
		}

		JSONArray rowLabels = json_obj.getJSONArray("row_labels");
		mRowLabels = new ArrayList<String>(rowLabels.length());
		for (int i = 0; i < rowLabels.length(); i++) {
			mRowLabels.add(rowLabels.getString(i));
		}

		mNames = Arrays.asList(mName.split("&"));
		mInputObj = json_obj;
	}

	private TitleQuestion generatePageBreakerQuestion()
			throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("name", "GridHeader");
		obj.put("title", mTitle);
		obj.put("desc", mDesc);
		obj.put("type", 10);
		obj.put("required", mRequired ? 1 : 0);
		return new TitleQuestion(mContext, mSurveyContext, obj);
	}

	private List<MultipleChoiceQuestion> generateMultipleQuestions()
			throws JSONException {
		List<MultipleChoiceQuestion> questions = new ArrayList<MultipleChoiceQuestion>(
				mNames.size());
		for (int i = 0; i < mNames.size(); i++) {
			JSONObject obj = new JSONObject();
			obj.put("name", mNames.get(i));
			if (mInputObj != null)
				obj.put("options", mInputObj.get("score_labels"));
			obj.put("title", mRowLabels.get(i));
			obj.put("required", mRequired ? 1 : 0);
			MultipleChoiceQuestion mcq = new MultipleChoiceQuestion(mContext,
					mSurveyContext, obj);
			mcq.mIsGrid = true;
			questions.add(mcq);
		}
		return questions;
	}

	public List<BaseQuestion> generateQuestions() {
		// return an page breaker & lots of multiple question
		List<BaseQuestion> bqList = new ArrayList<BaseQuestion>(
				1 + mNames.size());
		try {
			bqList.add(generatePageBreakerQuestion());
			bqList.addAll(generateMultipleQuestions());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return bqList;
	}

	@Override
	View fillContentView(ViewGroup container) {
		return null;
	}

	@Override
	boolean fillAnswer(ViewGroup container, String answer) {
		return false;
	}
}
