package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONObject;

import com.usercafe.utils.Base64;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;

public class ImageQuestion extends BaseQuestion {
	String mImageData;

	ImageQuestion(Context context, WeakReference<SurveyContext> surveyContext, JSONObject json_obj) {
		super(context, surveyContext, json_obj);
		mImageData = json_obj.optString("img_data");
	}

	@Override
	View fillContentView(ViewGroup container) {
		if (mImageData != null) {
			ImageView iv = new ImageView(mContext);
			byte[] data = Base64.decode(mImageData);
			if (data == null) {
				return null;
			}
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			iv.setImageBitmap(bitmap);
			iv.setScaleType(ScaleType.FIT_CENTER);
			iv.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			iv.setAdjustViewBounds(true);

			container.addView(iv);
			return iv;
		}
		return null;
	}

	@Override
	boolean fillAnswer(ViewGroup container, String answer) {
		return true;
	}
}
