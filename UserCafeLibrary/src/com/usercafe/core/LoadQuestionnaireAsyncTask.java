package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.usercafe.core.UserCafe.UserCafeReturnCode;
import com.usercafe.resource.UserCafeString;
import com.usercafe.utils.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

class LoadQuestionnaireAsyncTask extends AsyncTask<Void, Void, String> {
	static final String KEY_ERROR = "_ERROR";
	static final String FILLED = "_FILLED";
	static final int VAL_INVALID_KEY = 0;

	private Context mContext;
	private WeakReference<SurveyContext> mSurveyContext;
	private ProgressDialog mProgressDialog;

	public LoadQuestionnaireAsyncTask(Context context,
			WeakReference<SurveyContext> surveyContext) {
		super();
		this.mContext = context;
		this.mSurveyContext = surveyContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = ProgressDialog.show(mContext,
				UserCafeString.getString(UserCafeString.loading),
				UserCafeString.getString(UserCafeString.please_wait));
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();

		// error handling
		if (result == null) {
			mSurveyContext.get().closeWithReturnCode(UserCafeReturnCode.NETWORK_ERROR);
		}else if(result.equals(FILLED)){
			mSurveyContext.get().closeWithReturnCode(UserCafeReturnCode.ALREADY_FILLED);
		}else if (result.equals(KEY_ERROR)) {
			mSurveyContext.get().closeWithReturnCode(UserCafeReturnCode.INVALID_KEY);
		} else {
			mSurveyContext.get().mStateObj.mJSONString = result;
			mSurveyContext.get().openSurvey();
		}
	}

	@Override
	protected String doInBackground(Void... p) {
		try {
			HttpClient client = Utils.generateClient();
			String url = String.format(Constant.SERVER_URL
					+ "/api/app/%s/survey/%s/get_survey",
					mSurveyContext.get().mStateObj.mAppId,
					mSurveyContext.get().mStateObj.mSurveyId);
			HttpPost request = new HttpPost(url);
			final List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_device_id", PredefinedCustomData.deviceId(mContext)));
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			request.setEntity(entity);
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() == 204){
				// already filled
				return FILLED;
			}
			if (response.getStatusLine().getStatusCode() != 200) {
				return KEY_ERROR;
			} else {
				String jsonString = EntityUtils.toString(response.getEntity());
				return jsonString;
			}
		} catch (Exception e) {

		}
		return null;
	}
}
