package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.CustomRadioButton;
import com.usercafe.ui.UnscrollableListView;
import com.usercafe.utils.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

class MultipleChoiceQuestion extends BaseQuestion implements
		UnscrollableListView.OnItemClickListener {	
	private static final int OFFSET = 3000;
	private static final int RADIO_ID = OFFSET+1;

	
	ArrayList<String> mOptions;
	boolean mJumpPage;
	boolean mIsGrid;

	String TAG_PREFIX = "RadioBtn_";
	UnscrollableListView mLV;

	// other
	int mOtherIndex;
	boolean mDontJumpDialog;
	TextView mOtherTV;

	MultipleChoiceQuestion(Context context, WeakReference<SurveyContext> surveyContext, JSONObject json_obj)
			throws JSONException {
		super(context,surveyContext, json_obj);
		JSONArray optionArr = json_obj.getJSONArray("options");
		mJumpPage = false;
		mOther = false;
		mDontJumpDialog = false;
		mOtherIndex = -1;
		mIsGrid = false;
		mOptions = new ArrayList<String>(optionArr.length());
		for (int i = 0; i < optionArr.length(); i++) {
			mOptions.add(optionArr.getString(i));
		}
	}

	private void openOtherDialog(final Context context) {
		final EditText otherET = new EditText(context);
		if (mOtherStr != null) {
			otherET.setText(mOtherStr);
		}
		new AlertDialog.Builder(context).setTitle(UserCafeString.getString(UserCafeString.other_hint))
				.setView(otherET)
				.setNegativeButton(UserCafeString.getString(UserCafeString.cancel), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton(UserCafeString.getString(UserCafeString.ok), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						mOtherStr = otherET.getText().toString();
						mOtherTV.setText(String.format("%s : %s",
								UserCafeString.getString(UserCafeString.other_hint),
								mOtherStr));
					}
				}).show();
	}

	class MyListAdapter extends BaseAdapter {
		Context mContext;

		public MyListAdapter(Context context) {
			mContext = context;
		}

		@Override
		public int getCount() {
			return mOptions.size();
		}

		@Override
		public Object getItem(int position) {
			return mOptions.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			RelativeLayout rl = new RelativeLayout(mContext);
			rl.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT)); // 42
			int padding = Utils.getPxFromDip(mContext, 14);
			rl.setPadding(padding, padding, padding, padding);

			// cb
			CustomRadioButton rb = new CustomRadioButton(mContext, mSurveyContext.get().getColorObject().icon);
			int cbSize = Utils.getPxFromDip(mContext, 26);
			RelativeLayout.LayoutParams rbParams = new RelativeLayout.LayoutParams(cbSize,cbSize);
			rbParams.addRule(RelativeLayout.CENTER_VERTICAL);
			rbParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			rb.setLayoutParams(rbParams);
			rb.setId(RADIO_ID);
			rb.setTag(TAG_PREFIX + position);
			rb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					if (isChecked) {
						setAnswer((String) getItem(position));
						mOther = position == mOtherIndex;
					} else
						return;
					for (int i = 0; i < getCount(); i++) {
						if (i != position) {
							RadioButton rb = (RadioButton) mLV
									.findViewWithTag(TAG_PREFIX + i);
							rb.setChecked(false);
						}
					}

					// jump page
					if (mJumpPage) {
						int tempNext = mOwner.mNextPage;
						mOwner.mNextPage = mOwner.mJumpPage.get(mOptions
								.indexOf(getAnswer()));
						if (tempNext == mOwner.mNextPage) {
							return;
						}
						
						int currIdx = mOwner.mSurveyContext.get().getPageIndexByPageNumber(mOwner.mPageNum);
						int nextIdx = mOwner.mSurveyContext.get().getPageIndexByPageNumber(mOwner.mNextPage);
						
						boolean isChange = false;
						for (int idx = currIdx + 1; idx < nextIdx; idx++) {
							isChange |= mSurveyContext.get()
									.setHiddenToPageIndex(idx, true);
						}
						
						isChange |= mSurveyContext.get()
								.setHiddenToPageIndex(nextIdx, false);
						if (isChange) {
							 mSurveyContext.get().getQuestions(true);
						}
					}

					// other
					if (mOther) {
						if (mDontJumpDialog) {
							mDontJumpDialog = false;
						} else {
							openOtherDialog(mContext);
						}
					}
				}
			});
			rl.addView(rb);

			// text
			TextView textTV = new TextView(mContext);
			RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			textParams.addRule(RelativeLayout.CENTER_VERTICAL);
			textParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			textParams.addRule(RelativeLayout.LEFT_OF, RADIO_ID);
			textTV.setLayoutParams(textParams);
			textTV.setTextColor(mSurveyContext.get().getColorObject().button_text);
			textTV.setTextSize(16);
			textTV.setPadding(0, 0, Utils.getPxFromDip(mContext, 5), 0);
			// replace other
			String opt = (String) getItem(position);
			if (opt.equalsIgnoreCase(Constant.getOtherKey(mSurveyContext.get().getVersion()))) {
				mOtherTV = textTV;
				mOtherIndex = position;

				String otherStr = mOtherStr == null ? "" : " : " + mOtherStr;
				opt = String.format("%s%s",UserCafeString.getString(UserCafeString.other_hint), otherStr);
			}
			textTV.setText(opt);
			rl.addView(textTV);
			
			return rl;
		}
	}

	@Override
	View fillContentView(ViewGroup container) {
		mLV = new UnscrollableListView(mContext, Color.TRANSPARENT);
		mLV.setBackgroundColor(mSurveyContext.get().getColorObject().button_bg);
		mLV.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		mLV.setOnItemClickListener(this);
		mLV.setAdapter(new MyListAdapter(mContext));
		container.addView(mLV);
		return mLV;
	}

	@Override
	protected boolean fillAnswer(ViewGroup container, String answer) {
		if (answer == null)
			return true;
		int pos = mOptions.indexOf(answer);
		if (pos == -1) {
			return false;
		}
		if (pos == mOtherIndex) {
			mDontJumpDialog = true;
		}
		RadioButton rb = (RadioButton) mLV.findViewWithTag(TAG_PREFIX + pos);
		if (rb != null)
			rb.setChecked(true);
		return true;
	}

	@Override
	public void onItemClick(ViewGroup parent, View view, int position, Object o) {
		RadioButton rb = (RadioButton) mLV.findViewWithTag(TAG_PREFIX
				+ position);
		if (rb != null)
			rb.setChecked(true);
	}
}
