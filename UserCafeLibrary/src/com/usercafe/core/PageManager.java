package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

class PageManager implements Comparable<PageManager> {
	Context mContext;
	final int MODE_LAST = 0;
	final int MODE_MID = 1;
	int mPageNum;
	int mJumpIdx;
	int mDefaultNextPage;
	int mMode;
	List<Integer> mJumpPage;
	List<BaseQuestion> mQuestions;
	int mNextPage;
	int mPrevPage;
	boolean mHidden;
	WeakReference<SurveyContext> mSurveyContext;

	PageManager(Context context, WeakReference<SurveyContext> surveyContext,
			int pageNum, JSONObject json_obj) throws JSONException {
		mContext = context;
		mSurveyContext = surveyContext;
		mPageNum = pageNum;
		mJumpIdx = json_obj.getInt("jump_idx");
		mDefaultNextPage = json_obj.getInt("default_next_page");
		mMode = json_obj.getInt("mode");
		mHidden = false;

		JSONArray questions = json_obj.getJSONArray("questions");
		int questionLen = questions.length();
		JSONArray jumps = json_obj.getJSONArray("jump_page");
		int jumpLen = jumps.length();
		if (jumpLen != 0) {
			mJumpPage = new ArrayList<Integer>(jumpLen);
			for (int i = 0; i < jumpLen; i++) {
				Integer page = jumps.getInt(i);
				mJumpPage.add(page);
			}
		} else {
			mJumpPage = null;
		}

		String page_breaker = null;
		String section = null;
		String grid_title = null;
		mQuestions = new ArrayList<BaseQuestion>();
		for (int i = 0; i < questionLen; i++) {
			boolean needAdded = true;
			JSONObject obj = questions.getJSONObject(i);
			List<BaseQuestion> bqList = QuestionFactory.createQuestion(
					mContext, mSurveyContext, obj);
			if (bqList == null)
				continue;
			for (int j = 0; j < bqList.size(); j++) {
				BaseQuestion bq = bqList.get(j);
				if (bq.mType == QuestionnaireType.TYPE_PAGE_BREAKER.ordinal()) { // page
																					// breaker
																					// TODO
																					// define
					page_breaker = bq.mTitle;
					section = null;
					grid_title = null;
					if (bq.mDesc == null || bq.mDesc.length() == 0)
						needAdded = false;
				} else if (bq.mType == QuestionnaireType.TYPE_SECTION_HEADER
						.ordinal()) { // section
					section = bq.mTitle;
					bq.mFirstLayer = page_breaker;
					grid_title = null;
					if (bq.mDesc == null || bq.mDesc.length() == 0)
						needAdded = false;
				} else if (bq.mType == QuestionnaireType.TYPE_SUBMIT.ordinal()) {
					bq.mFirstLayer = bq.mSecondLayer = bq.mThirdLayer = null;
				} else {
					bq.mFirstLayer = page_breaker;
					if (bq.mFirstLayer == null) {
						// no page breaker
						bq.mFirstLayer = section;
					} else {
						bq.mSecondLayer = section;
					}

					if (bq instanceof MultipleChoiceQuestion
							&& ((MultipleChoiceQuestion) bq).mIsGrid) {
						if (bq.mFirstLayer == null) {
							bq.mFirstLayer = grid_title;
						} else if (bq.mSecondLayer == null) {
							bq.mSecondLayer = grid_title;
						} else {
							bq.mThirdLayer = grid_title;
						}
					}

					if (bq.mType == 10) { // grid title [TODO] ???
						grid_title = bq.mTitle;
					}
				}

				// jump page
				if (mJumpIdx == i) {
					if (!(bq instanceof MultipleChoiceQuestion)) {
						// TODO: error handling
					} else {
						((MultipleChoiceQuestion) bq).mJumpPage = true;
					}
				}
				bq.mOwner = this;

				// last
				if (i == questionLen - 1) {
					bq.mLastQuestion = true;
				}

				// first
				if (i == 0) {
					bq.mFirstQuestion = true;
				}
				if (needAdded)
					mQuestions.add(bq);
			}
		}
		mNextPage = mDefaultNextPage;
	}

	// private String getCacheRelatedKey(){
	// int version = QuestionContext.sharedContext().getVersion();
	// switch(version){
	// case 1:
	// return "backupCache";
	// case 2:
	// return "draftResponse";
	// }
	// return null;
	// }
	//
	// private String getPageRelatedKey(){
	// int version = QuestionContext.sharedContext().getVersion();
	// switch(version){
	// case 1:
	// return "pageNumber";
	// case 2:
	// return "pageHistory";
	// }
	// return null;
	// }

	@Override
	public int compareTo(PageManager another) {
		return mPageNum - another.mPageNum;
	}
}
