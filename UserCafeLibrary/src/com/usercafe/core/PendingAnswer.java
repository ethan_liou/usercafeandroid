package com.usercafe.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.usercafe.utils.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

abstract class PendingAnswer {
	public static Boolean SendAnswerToServer(Context mContext, String appId,
			String surveyId, List<NameValuePair> params) {
		try {
			HttpClient client = Utils.generateClient();
			String url = String.format(Constant.SERVER_URL
					+ "/api/app/%s/survey/%s", appId, surveyId);
			HttpPost post = new HttpPost(url);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	static Boolean sendPendingAnswers(Context context, int tid, String appId,
			String surveyId) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			Cursor cursor = db.rawQuery(
					"SELECT _key,_value FROM PendingAnswer WHERE _tid=?",
					new String[] { "" + tid });
			List<NameValuePair> params = new ArrayList<NameValuePair>(
					cursor.getCount());
			while (cursor.moveToNext()) {
				params.add(new BasicNameValuePair(cursor.getString(0), cursor
						.getString(1)));
			}

			Boolean ret = SendAnswerToServer(context, appId, surveyId, params);
			if (ret) {
				db.delete("PendingAnswer", "_tid=?", new String[] { "" + tid });
				db.delete("TransactionTable", "_tid=?",
						new String[] { "" + tid });
			}
			db.close();
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	static void sendAllPendingAnswer(Context context) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getReadableDatabase();
			Cursor cursor = db.rawQuery("SELECT * FROM TransactionTable", null);
			while (cursor.moveToNext()) {
				int tid = cursor.getInt(0);
				String appId = cursor.getString(1);
				String surveyId = cursor.getString(2);
				sendPendingAnswers(context, tid, appId, surveyId);
			}
			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static int createPendingAnswer(Context context, String surveyId,
			String appId, List<NameValuePair> params) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			ContentValues values = new ContentValues(3);
			values.put("_app_id", appId);
			values.put("_survey_id", surveyId);
			values.put("_time", System.currentTimeMillis());
			long tid = db.insert("TransactionTable", null, values);
			for (NameValuePair pair : params) {
				values = new ContentValues(3);
				values.put("_tid", tid);
				values.put("_key", pair.getName());
				values.put("_value", pair.getValue() == null ? "Unknown" : pair.getValue());
				db.insert("PendingAnswer", null, values);
			}
			db.close();
			return (int) tid;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
}
