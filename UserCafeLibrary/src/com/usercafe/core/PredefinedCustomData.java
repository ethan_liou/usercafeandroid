package com.usercafe.core;

import java.lang.reflect.Method;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public abstract class PredefinedCustomData {
	public static String callFuncByName(Context context, String name) {
		try {
			Method method = PredefinedCustomData.class.getDeclaredMethod(name,
					Context.class);
			String ret = (String) method.invoke(null, context);
			return ret == null ? "" : ret;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String deviceId(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm != null) {
			return tm.getDeviceId();
		}
		return null;
	}

	public static String deviceModel(Context context) {
		return Build.MODEL;
	}

	public static String cellId(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm != null) {
			GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
			if (location != null)
				return "" + location.getCid() + "," + location.getLac();
		}
		return null;
	}

	public static String wifiBSSID(Context context) {
		WifiManager wm = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		if (wm != null && wm.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
			WifiInfo info = wm.getConnectionInfo();
			if (info != null)
				return info.getBSSID();
		}
		return null;
	}

	public static String carrierNumber(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String num = null;
		if (tm != null) {
			num = tm.getSimOperator();
			if (num == null || num.length() == 0)
				num = tm.getNetworkOperator();
		}
		if( num != null ){
			num = num.substring(0, 3) + "," + num.substring(3);
		}
		return num;
	}

	public static String carrierName(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm != null) {
			String simName = tm.getSimOperatorName();
			if (simName == null || simName.length() == 0)
				return tm.getNetworkOperatorName();
			return simName;
		}
		return null;
	}
}
