package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

enum QuestionnaireType {
	TYPE_TEXT, TYPE_LONG_TEXT, TYPE_MULTIPLECHOICE, TYPE_CHECKBOX, TYPE_LIST, TYPE_SCALE, TYPE_GRID, TYPE_PAGE_BREAKER, TYPE_SECTION_HEADER, TYPE_SUBMIT, TYPE_IMAGE, TYPE_DATE, TYPE_TIME
};

class QuestionFactory {
	static List<BaseQuestion> createQuestion(Context context,
			WeakReference<SurveyContext> surveyContext, JSONObject jsonObj) {
		try {
			BaseQuestion bq = null;
			int type = jsonObj.getInt("type");
			if (type >= QuestionnaireType.values().length) {
				return null;
			}
			QuestionnaireType qType = QuestionnaireType.values()[type];
			switch (qType) {
			case TYPE_TEXT:
			case TYPE_LONG_TEXT:
				bq = new TextQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_LIST:
			case TYPE_MULTIPLECHOICE:
				bq = new MultipleChoiceQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_CHECKBOX:
				bq = new CheckBoxQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_SCALE:
				bq = new ScoreQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_GRID:
				bq = new GridQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_PAGE_BREAKER: // page breaker
			case TYPE_SECTION_HEADER: // section header
				bq = new TitleQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_SUBMIT:
				bq = new SubmitQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_IMAGE:
				bq = new ImageQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_DATE:
				bq = new DateQuestion(context,surveyContext, jsonObj);
				break;
			case TYPE_TIME:
				bq = new TimeQuestion(context,surveyContext, jsonObj);
				break;
			default:
				return null;
			}
			if (bq != null && qType != QuestionnaireType.TYPE_GRID) {
				List<BaseQuestion> bqList = new ArrayList<BaseQuestion>(1);
				bqList.add(bq);
				return bqList;
			} else if (bq != null && bq instanceof GridQuestion) {
				List<BaseQuestion> bqList = new ArrayList<BaseQuestion>();
				bqList.addAll(((GridQuestion) bq).generateQuestions());
				return bqList;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
