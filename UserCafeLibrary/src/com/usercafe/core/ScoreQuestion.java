package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONException;
import org.json.JSONObject;

import com.usercafe.ui.RectDrawable;
import com.usercafe.ui.ScoreLayout;
import com.usercafe.ui.SquareTextView;
import com.usercafe.utils.Utils;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;

class ScoreQuestion extends BaseQuestion implements OnItemClickListener {
	String mLeftText;
	String mRightText;
	int mMaxStore;
	ScoreAdapter mSA;

	ScoreQuestion(Context context, WeakReference<SurveyContext> surveyContext,
			JSONObject json_obj) throws JSONException {
		super(context, surveyContext, json_obj);
		mLeftText = json_obj.getString("left_label");
		mRightText = json_obj.getString("right_label");
		mMaxStore = json_obj.getInt("max_score");
	}

	@Override
	View fillContentView(ViewGroup container) {
		ScoreLayout sl = new ScoreLayout(mContext, mMaxStore, mSurveyContext
				.get().getColorObject().text);

		// [TODO] this is workaround...
		String tmpLeftText = mLeftText;
		int pos = mLeftText.lastIndexOf("\n");
		if (pos >= 0) {
			tmpLeftText = mLeftText.substring(0, pos);
		}
		String tmpRightText = mRightText;
		pos = mRightText.lastIndexOf("\n");
		if (pos >= 0) {
			tmpRightText = mRightText.substring(0, pos);
		}

		sl.setLeftText(tmpLeftText);
		sl.setRightText(tmpRightText);
		mSA = new ScoreAdapter(mContext, mSurveyContext, mMaxStore);
		sl.setAdapter(mSA);
		sl.setOnItemClickListener(this);
		container.addView(sl);
		return sl;
	}

	@Override
	protected boolean fillAnswer(ViewGroup container, String answer) {
		if (answer == null)
			return true;
		mSA.setScore(Integer.parseInt(answer));
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		setAnswer("" + (position + 1));
		mSA.setScore(position + 1);
	}
}

class ScoreAdapter extends BaseAdapter {
	int mCount;
	Context mContext;
	WeakReference<SurveyContext> mSurveyContext;
	int mScore;

	public ScoreAdapter(Context context,
			WeakReference<SurveyContext> surveyContext, int count) {
		super();
		mContext = context;
		mSurveyContext = surveyContext;
		mCount = count;
		mScore = 0;
	}

	public void setScore(int score) {
		mScore = score;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public Object getItem(int position) {
		return "" + (position + 1);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SquareTextView stv;
		if (convertView == null) {
			stv = new SquareTextView(mContext);
			stv.setGravity(Gravity.CENTER);
		} else {
			stv = (SquareTextView) convertView;
		}
		stv.setText((String) getItem(position));
		stv.setTextColor(position == mScore - 1 ? mSurveyContext.get()
				.getColorObject().scale_active_text : mSurveyContext.get()
				.getColorObject().scale_inactive_text);
		Utils.setBackgroundDrawable(stv,
				new RectDrawable(position == mScore - 1 ? 
						mSurveyContext.get().getColorObject().scale_active_bg :
						mSurveyContext.get().getColorObject().scale_inactive_bg));
		return stv;
	}

}