package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONObject;

import com.usercafe.resource.UserCafeImage;
import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.RectDrawable;
import com.usercafe.utils.Base64;
import com.usercafe.utils.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

class SubmitQuestion extends BaseQuestion implements OnClickListener {
	Context mContext;

	SubmitQuestion(Context context, WeakReference<SurveyContext> surveyContext, JSONObject json_obj) {
		super(context,surveyContext, json_obj);
		mName = "_submit";
		mTitle = UserCafeString.getString(UserCafeString.submit);
		mContext = context;
		mDesc = surveyContext.get().getSubmitDesc();
	}

	@Override
	void modifyTitleAndDesc(TextView titleTV, TextView descTV) {
		titleTV.setGravity(Gravity.CENTER_HORIZONTAL);
		descTV.setGravity(Gravity.CENTER_HORIZONTAL);
	}

	@Override
	protected View fillContentView(ViewGroup container) {
		Button submitBtn = new Button(mContext);
		Utils.setBackgroundDrawable(submitBtn, new RectDrawable(mSurveyContext.get().getColorObject().button_bg));
		submitBtn.setOnClickListener(this);
		submitBtn.setTextColor(mSurveyContext.get().getColorObject().button_text);
		submitBtn.setText(UserCafeString.getString(UserCafeString.send));
		submitBtn.setOnClickListener(this);
		setAnswer("Submit");
		Button promoBtn = new Button(mContext);
		promoBtn.setBackgroundColor(Color.TRANSPARENT);
		promoBtn.setText("User Cafe");
		
		LinearLayout pw = new LinearLayout(mContext);
		byte[] data = Base64.decode(UserCafeImage.DARK_LOGO);
		int height = Utils.getPxFromDip(mContext, 28);
		int width = height * 200 / 195;
		Bitmap scaleBitmap = Utils.scaleBitmap(data, width, height);
		BitmapDrawable bd = new BitmapDrawable(mContext.getResources(),scaleBitmap);
		promoBtn.setCompoundDrawablesWithIntrinsicBounds(bd, null, null, null);
		promoBtn.setCompoundDrawablePadding(10);
		
		TextView promo = new TextView(mContext);
		promo.setText("powered by ");
		pw.addView(promo);
		pw.addView(promoBtn);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		pw.setLayoutParams(params);
		
		container.addView(submitBtn);
		container.addView(pw);
		return submitBtn;
	}

	@Override
	public void onClick(View v) {
		mSurveyContext.get().sendAnswerToServer(true);
	}

	@Override
	protected boolean fillAnswer(ViewGroup container, String answer) {
		return true;
	}
}
