package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.List;

import org.apache.http.NameValuePair;

import com.usercafe.core.SurveyContext.SurveyStatus;
import com.usercafe.core.UserCafe.UserCafeReturnCode;
import com.usercafe.resource.UserCafeString;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

class SubmitSurveyAsyncTask extends
		AsyncTask<List<NameValuePair>, Void, Boolean> {
	private ProgressDialog mProgressDialog;
	private Context mContext;
	private WeakReference<SurveyContext> mSurveyContext;
	
	public SubmitSurveyAsyncTask(Context mContext,
			WeakReference<SurveyContext> mSurveyContext) {
		super();
		this.mContext = mContext;
		this.mSurveyContext = mSurveyContext;
	}

	@Override
	protected Boolean doInBackground(List<NameValuePair>... params) {
		if (params == null || params.length == 0)
			return false;
		String appId = mSurveyContext.get().mStateObj.mAppId;
		String surveyId = mSurveyContext.get().mStateObj.mSurveyId;
		int tid = PendingAnswer.createPendingAnswer(mContext, surveyId, appId, params[0]);
		boolean ret = false;
		if( tid >= 0){
			ret = PendingAnswer.sendPendingAnswers(mContext, tid, appId, surveyId);
			mSurveyContext.get().mCurrentStatus = ret ? SurveyStatus.SENT : SurveyStatus.NOT_SENT;
		}
		return ret;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = ProgressDialog.show(mContext,
				UserCafeString.getString(UserCafeString.loading),
				UserCafeString.getString(UserCafeString.please_wait));
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		mSurveyContext.get().closeWithReturnCode(
				result ? UserCafeReturnCode.OK
						: UserCafeReturnCode.NETWORK_ERROR);
	}
}
