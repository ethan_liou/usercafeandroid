package com.usercafe.core;

import com.usercafe.core.UserCafe.CacheMode;
import com.usercafe.core.UserCafe.OnUserCafeCallback;
import com.usercafe.core.UserCafe.UserCafeReturnCode;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class SurveyActivity extends Activity {
	SurveyContext mSurveyContext;
	boolean mSendAnswer;
	@Override
	public void onBackPressed() {
		mSurveyContext.confirmExit();
	}
	
	@Override
	protected void onStop() {
		if(mSendAnswer && mSurveyContext != null){
			try{
				mSurveyContext.sendAnswerToServer(false);				
			}catch(Exception e){
				// pass
			}
		}
		super.onStop();
	}

	@Override
	protected void onStart() {
		super.onStart();
		mSendAnswer = true;
	}

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(Build.VERSION.SDK_INT >= 14){
			setTheme(android.R.style.Theme_Holo_Light_NoActionBar);
		}
		else if(Build.VERSION.SDK_INT >= 11){
			setTheme(android.R.style.Theme_Holo_Light);
		}
		else{
			setTheme(android.R.style.Theme_Light_NoTitleBar);
		}
		super.onCreate(savedInstanceState);
		UserCafeStateObject stateObj = getIntent().getParcelableExtra(
				"stateObj");
		if (stateObj == null) {
			finish();
		} else {
			UserCafe.sharedInstance().AppCreate(this, stateObj.mAppId, null);
			mSurveyContext = UserCafe.sharedInstance().openSurvey(this, stateObj.mSurveyId,
					stateObj.mSubmitDesc,
					CacheMode.values()[stateObj.mCacheMode + 2],
					stateObj.mCustomData, stateObj.mCustomColor, new OnUserCafeCallback() {
						@Override
						public void onReturn(UserCafeReturnCode code) {
							if(code != UserCafeReturnCode.USER_STOP)
								mSendAnswer = false;
							setResult(code.getValue());
							finish();
						}

						@Override
						public void onLoadingFinish(View view) {
							setContentView(view);
						}
					});
		}
	}
}
