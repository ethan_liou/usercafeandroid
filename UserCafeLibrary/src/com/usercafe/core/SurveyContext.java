package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.usercafe.core.UserCafe.OnUserCafeCallback;
import com.usercafe.core.UserCafe.UserCafeReturnCode;
import com.usercafe.deviceinfo.AbstractInfo;
import com.usercafe.deviceinfo.DeviceInfoFactory;
import com.usercafe.resource.UserCafeColor;
import com.usercafe.resource.UserCafeString;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

// survey context
class SurveyContext {
	WeakReference<UserCafe> mUserCafe;
	List<PageManager> mPages;
	private int mVersion;
	long mStartTime;
	UserCafeStateObject mStateObj;

	Context mContext;
	OnUserCafeCallback mUserCafeCallback;
	AnswerCache mAnswerCache;
	SurveyStatus mCurrentStatus;
	SurveyInfo mInitInfo;
	SurveyView mView;
	String mTitle;
	boolean mSentAnswer;
	
	// device info
	List<AbstractInfo > mDeviceInfos;

	enum SurveyStatus {
		NOT_FILL, NOT_FINISH, NOT_SENT, SENT
	}

	public UserCafeColor getColorObject(){
		if(mStateObj.mCustomColor == null){
			mStateObj.mCustomColor = new UserCafeColor();
		}
		return mStateObj.mCustomColor;
	}
	
	public int getPageIndexByPageNumber(int number) {
		for (PageManager pm : mPages) {
			if (pm.mPageNum == number) {
				return mPages.indexOf(pm);
			}
		}
		return -1;
	}

	class SurveyInfo {
		SurveyStatus mStatus;
		long mTime;
		String mStatusData;

		@Override
		public String toString() {
			return "SurveyInfo [mStatus=" + mStatus + ", mTime=" + mTime
					+ ", mStatusData=" + mStatusData + "]";
		}
	}

	int GetRealPos() {
		return mView.mLastQuestion.mRealPos;
	}

	SurveyContext(Context context, UserCafeStateObject stateObj,
			WeakReference<UserCafe> userCafe, OnUserCafeCallback listener) {
		mContext = context;
		mStateObj = stateObj;
		mUserCafe = userCafe;
		mUserCafeCallback = listener;
	}

	SurveyInfo getSurveyInfo(Context context) {
		try {
			SurveyInfo info = null;
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getReadableDatabase();
			Cursor cursor = db.rawQuery(
					"SELECT * FROM Survey WHERE _survey_id =? and _app_id =?",
					new String[] { mStateObj.mSurveyId, mStateObj.mAppId });
			if (cursor.getCount() > 0) {
				cursor.moveToNext();
				info = new SurveyInfo();
				info.mStatus = SurveyStatus.values()[cursor.getInt(cursor
						.getColumnIndex("_status"))];
				info.mStatusData = cursor.getString(cursor
						.getColumnIndex("_status_data"));
				info.mTime = cursor.getLong(cursor.getColumnIndex("_time"));
			}
			db.close();
			return info;
		} catch (Exception e) {
			// pass
		}
		return null;
	}

	void loadSurveyFromServer() {
		LoadQuestionnaireAsyncTask task = new LoadQuestionnaireAsyncTask(
				mContext, new WeakReference<SurveyContext>(this));
		task.execute();
	}

	SurveyInfo addSurveyToLocalDB(Context context) {
		try {
			SurveyInfo info = null;
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			ContentValues values = new ContentValues(4);
			values.put("_survey_id", mStateObj.mSurveyId);
			values.put("_app_id", mStateObj.mAppId);
			values.put("_status", SurveyStatus.NOT_FILL.ordinal());
			values.put("_time", System.currentTimeMillis());
			long row_id = db.insert("Survey", null, values);
			if (row_id >= 0) {
				info = new SurveyInfo();
				info.mStatus = SurveyStatus.NOT_FILL;
				info.mTime = System.currentTimeMillis();
			}
			db.close();
			return info;
		} catch (Exception e) {
			// pass
		}
		return null;
	}

	void saveSurveyToLocalDB(Context context) {
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getWritableDatabase();
			ContentValues values = new ContentValues(6);
			values.put("_survey_id", mStateObj.mSurveyId);
			values.put("_app_id", mStateObj.mAppId);
			values.put("_status", mCurrentStatus.ordinal());
			values.put("_time", System.currentTimeMillis());
			values.put("_status_data", mInitInfo.mStatusData);
			db.replace("Survey", null, values);
			db.close();
		} catch (Exception e) {
			// pass
		}
	}

	void closeWithReturnCode(UserCafeReturnCode returnCode) {
		mUserCafeCallback.onReturn(returnCode);
		if (mView == null || mView.mPager == null) {
			if (mUserCafe != null)
				mUserCafe.get().closeSurvey(this);
			return;
		} else if (returnCode == UserCafeReturnCode.OK) {
			mCurrentStatus = SurveyStatus.SENT;
		} else if (mView.mPager.getCurrentItem() != 0) {
			mCurrentStatus = SurveyStatus.NOT_FINISH;
			mInitInfo.mStatusData = mView.mLastQuestion.mName;
		} else {
			mCurrentStatus = SurveyStatus.NOT_FILL;
		}
		saveSurveyToLocalDB(mContext);

		if (mAnswerCache != null) {
			mAnswerCache.commit(mContext);
		}

		mView = null;

		if (mUserCafe != null)
			mUserCafe.get().closeSurvey(this);
	}

	private boolean prepareSurvey() {
		JSONObject json_obj = null;
		try {
			json_obj = new JSONObject(mStateObj.mJSONString);
			mStateObj.mSurveyId = json_obj.getString("_id");
			mInitInfo = getSurveyInfo(mContext);
			if (mInitInfo == null) {
				mInitInfo = addSurveyToLocalDB(mContext);
			}
			if (mInitInfo == null) {
				mInitInfo = new SurveyInfo();
				mInitInfo.mStatus = SurveyStatus.NOT_FILL;
				mInitInfo.mStatusData = "";
				mInitInfo.mTime = System.currentTimeMillis();
			}
			mCurrentStatus = mInitInfo.mStatus;
			mAnswerCache = new AnswerCache(mStateObj.mSurveyId);
			if (mStateObj.mCacheMode >= 0) {
				mAnswerCache.loadAllCacheToMap(mContext);
			}
			if (mStateObj.mCacheMode == -2) {
				mAnswerCache.clearCache(mContext);
			}

			mTitle = json_obj.optString("title", "");
			String desc = json_obj.optString("desc", "");
			mVersion = json_obj.optInt("version", 1);
			
			// properties
			JSONObject properties = json_obj.optJSONObject("properties");
			if(properties != null){
				JSONArray device_info = properties.optJSONArray("device_info");
				if(device_info != null){
					if(mStateObj.mCustomData == null){
						mStateObj.mCustomData = new HashMap<String, String>();
					}
					String [] device_infos = new String[device_info.length()];
					for(int i = 0 ; i < device_info.length() ; i++){
						device_infos[i] = device_info.optString(i);
					}
					mDeviceInfos = DeviceInfoFactory.generateInfos(mContext, device_infos);
					// load every value
					new Thread(new Runnable(){
						@Override
						public void run() {
							for(AbstractInfo info : mDeviceInfos){
								info.loadValue();
							}
						}
					}).start();
				}
			}

			JSONObject pageDict = json_obj.getJSONObject("pages");
			Iterator<?> iter = pageDict.keys();
			mPages = new ArrayList<PageManager>(pageDict.length());
			while (iter.hasNext()) {
				String pageNumStr = (String) iter.next();
				PageManager pm = new PageManager(mContext,
						new WeakReference<SurveyContext>(this),
						Integer.parseInt(pageNumStr),
						pageDict.optJSONObject(pageNumStr));
				mPages.add(pm);
			}
			Collections.sort(mPages);

			// insert title to first page
			JSONObject titleObj = new JSONObject();
			titleObj.put("title", "");
			titleObj.put("desc", desc == null ? "" : desc);
			TitleQuestion tq = new TitleQuestion(mContext,
					new WeakReference<SurveyContext>(this), titleObj);
			mPages.get(0).mQuestions.add(0, tq);
			int pos = 0;
			for (PageManager pm : mPages) {
				for (BaseQuestion bq : pm.mQuestions) {
					bq.mPos = pos++;
				}
			}
			return true;
		} catch (Exception e) {

		}
		return false;
	}

	void confirmExit() {
		new AlertDialog.Builder(mContext)
				.setTitle(UserCafeString.getString(UserCafeString.back_title))
				.setNegativeButton(
						UserCafeString.getString(UserCafeString.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						})
				.setPositiveButton(
						UserCafeString.getString(UserCafeString.back_ok),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								closeWithReturnCode(UserCafeReturnCode.USER_STOP);
							}
						}).show();
	}

	void loadCache(final Context context) {
		if (mStateObj.mCacheMode > 0) {
			mView.mPager.setCurrentItem(mStateObj.mCacheMode, false);

		} else if (mStateObj.mCacheMode == -1
				&& mCurrentStatus == SurveyStatus.NOT_FINISH) {
			new AlertDialog.Builder(context)
					.setTitle(
							UserCafeString
									.getString(UserCafeString.required_title))
					.setMessage(
							String.format(UserCafeString
									.getString(UserCafeString.unfinish_msg),
									UserCafeString.getDiffTimeStr(System
											.currentTimeMillis()
											- mInitInfo.mTime)))
					.setPositiveButton(
							UserCafeString.getString(UserCafeString.ok),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									mAnswerCache.loadAllCacheToMap(context);
									// reload first quesiton's answer
									BaseQuestion firstQ = getQuestions(false)
											.get(0);
									firstQ.loadAnswer();
									firstQ.fillAnswer(
											firstQ.mBaseView.mContentLayout,
											firstQ.getAnswer());

									if (mInitInfo.mStatusData != null) {
										// init name
										List<BaseQuestion> bqs = getQuestions(false);
										for (BaseQuestion bq : bqs) {
											if (bq.mName != null
													&& bq.mName
															.equalsIgnoreCase(mInitInfo.mStatusData)) {
												mView.mPager.setCurrentItem(
														bq.mRealPos, true);
												break;
											}
										}
									}
								}
							})
					.setNegativeButton(
							UserCafeString.getString(UserCafeString.cancel),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									mAnswerCache.clearCache(context);
									mCurrentStatus = SurveyStatus.NOT_FILL;
								}
							}).show();
		}

	}

	void openSurvey() {
		boolean ret = prepareSurvey();
		if (ret) {
			mStartTime = System.currentTimeMillis();
			mView = new SurveyView(mContext, new WeakReference<SurveyContext>(
					this));
			mView.mPager.setAdapter(new PagerAdapter() {
				@Override
				public void destroyItem(View container, int position,
						Object object) {
					((ViewPager) container).removeView((View) object);
				}

				@Override
				public boolean isViewFromObject(View arg0, Object arg1) {
					return arg0 == arg1;
				}

				@Override
				public int getItemPosition(Object object) {
					return POSITION_NONE;
				}

				@Override
				public int getCount() {
					return getQuestions(false).size();
				}

				@Override
				public Object instantiateItem(View container, int position) {
					BaseQuestion bq = getQuestions(false).get(position);
					View v = bq.renderView();
					if (v.getParent() == null)
						((ViewPager) container).addView(v);
					return v;
				}

			});
			// first question
			List<BaseQuestion> questions = getQuestions(false);
			mView.mLastQuestion = questions.get(0);
			mView.setMaxProgress(getQuestions(false).size());
			mView.mTitleView.setText(mTitle);

			// view is ready
			mUserCafeCallback.onLoadingFinish(mView);

			loadCache(mContext);
		} else {
			closeWithReturnCode(UserCafeReturnCode.SERVER_ERROR);
		}
	}

	int getVersion() {
		return mVersion;
	}

	int findMaxPageNum() {
		int maxNum = 0;
		for (PageManager pm : mPages) {
			maxNum = pm.mPageNum > maxNum ? pm.mPageNum : maxNum;
		}
		return maxNum;
	}

	boolean setHiddenToPageIndex(int index, boolean hidden) {
		if (index < 0) {
			// last page
			return false;
		}
		PageManager pm = mPages.get(index);
		if (pm.mHidden == hidden) {
			return false;
		}
		pm.mHidden = hidden;
		return true;
	}

	List<BaseQuestion> mQuestions = null;

	List<BaseQuestion> getQuestions(boolean reload) {
		if (!reload && mQuestions != null)
			return mQuestions;
		if (mQuestions == null)
			mQuestions = new ArrayList<BaseQuestion>();
		else
			mQuestions.clear();
		int index = 0;
		for (PageManager pm : mPages) {
			if (!pm.mHidden) {
				for (BaseQuestion bq : pm.mQuestions) {
					bq.mRealPos = index++;
					mQuestions.add(bq);
				}
			}
		}
		mView.mPager.getAdapter().notifyDataSetChanged();
		return mQuestions;
	}

	private String replacePointToUnderline(String s) {
		return s.replace(".", "_");
	}

	@SuppressWarnings("unchecked")
	void sendAnswerToServer(boolean finished) {
		final String KEY_APP_ID = "_app_id";
		final String KEY_SURVEY_ID = "_survey_id";
		final String KEY_DEVICE_ID = "_device_id";
		final String KEY_FINISH_TIME = "_finish_time";
		final String KEY_FINISHED = "_finished";

		final List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_SURVEY_ID, mStateObj.mSurveyId));
		params.add(new BasicNameValuePair(KEY_APP_ID, mStateObj.mAppId));
		params.add(new BasicNameValuePair(KEY_FINISH_TIME, ""
				+ (System.currentTimeMillis() - mStartTime) / 1000));

		// finished
		params.add(new BasicNameValuePair(KEY_FINISHED, ""+finished));
		
		// device_id
		params.add(new BasicNameValuePair(KEY_DEVICE_ID, PredefinedCustomData.deviceId(mContext)));
		
		// device info
		if(mDeviceInfos != null){
			for(AbstractInfo device_info : mDeviceInfos ){
				String value = device_info.getValue();
				if(value != null){
					params.add(new BasicNameValuePair("_"+device_info.getName(), value));
				}
			}
		}

		// answer
		for (PageManager pm : mPages) {
			for (BaseQuestion bq : pm.mQuestions) {
				if (bq.mName != null && bq.mName.length() != 0 && bq.mAnswer != null
						&& bq.mType != QuestionnaireType.TYPE_SUBMIT.ordinal()) {
					String answer = bq.mAnswer;
					if (bq.mOther && bq.mOtherStr != null) {
						answer = answer.replace(Constant.getOtherKey(mVersion),
								bq.mOtherStr);
					}
//					answer = answer.replace("\n", " ");
					params.add(new BasicNameValuePair(
							replacePointToUnderline(bq.mName), answer));
					params.add(new BasicNameValuePair(
							replacePointToUnderline(bq.mName + "_t"), ""
									+ bq.mStopTime));
				}
			}
		}

		// custom
		if (mStateObj.mCustomData != null) {
			for (String key : mStateObj.mCustomData.keySet()) {
				params.add(new BasicNameValuePair(key, mStateObj.mCustomData
						.get(key)));
			}
		}

		if(finished){
			new SubmitSurveyAsyncTask(mContext, new WeakReference<SurveyContext>(
				this)).execute(params);
		}
		else{
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					int tid = PendingAnswer.createPendingAnswer(mContext, mStateObj.mSurveyId, mStateObj.mAppId, params);
					if(tid >= 0){
						PendingAnswer.sendPendingAnswers(mContext, tid, mStateObj.mAppId, mStateObj.mSurveyId);
					}
				}
			});
			t.setDaemon(true);
			t.start();
		}
	}

	public String getSubmitDesc() {
		String submitString = "";
		if (mStateObj != null && mStateObj.mSubmitDesc != null)
			submitString = mStateObj.mSubmitDesc;
		return submitString;
	}
}
