package com.usercafe.core;

import java.lang.ref.WeakReference;

import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.AutoResizeTextView;
import com.usercafe.ui.BackImageButton;
import com.usercafe.ui.RectDrawable;
import com.usercafe.utils.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SurveyView extends RelativeLayout implements OnPageChangeListener {
	// id
	private static final int OFFSET_ID = 2000;
	static final int PROGRESS_ID = OFFSET_ID + 1;
	static final int ACTIONBAR_ID = OFFSET_ID + 2;
	static final int CONTENT_ID = OFFSET_ID + 3;
	static final int LEVEL1_ID = OFFSET_ID + 4;
	static final int LEVEL2_ID = OFFSET_ID + 5;
	static final int LEVEL3_ID = OFFSET_ID + 6;
	static final int BTN_AREA = OFFSET_ID + 7;

	// actionbar
	private RelativeLayout actionbarArea;
//	private View actionbarBottomLine;
	AutoResizeTextView mTitleView;
	BackImageButton mBackBtn;

	// prev , next btn
	Button mNextBtn;
	Button mPrevBtn;

	// progress bar
	ProgressBar mProgressBar;

	// content
	LinearLayout contentArea;
	TextView mFirstLayer;
	TextView mSecondLayer;
	TextView mThirdLayer;
	ViewPager mPager;

	BaseQuestion mLastQuestion;
	WeakReference<SurveyContext> mSurveyContext;

	public SurveyView(Context context) {
		super(context);
		throw (new UnsupportedOperationException());
	}

	public SurveyView(Context context,
			WeakReference<SurveyContext> surveyContext) {
		super(context);
		mSurveyContext = surveyContext;
		setBackgroundColor(mSurveyContext.get().getColorObject().bg);
		// setup actionbar
		{
			int actionbarHeight = Utils.getPxFromDip(context, 48);

			actionbarArea = new RelativeLayout(context);
			actionbarArea.setLayoutParams(new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT, actionbarHeight));
			actionbarArea.setBackgroundColor(mSurveyContext.get().getColorObject().actionbar_bg);

			// bottom line
//			actionbarBottomLine = new View(context);
//			RelativeLayout.LayoutParams bottomParams = new RelativeLayout.LayoutParams(
//					RelativeLayout.LayoutParams.MATCH_PARENT,
//					Utils.getPxFromDip(context, 2));
//			bottomParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//			actionbarBottomLine.setLayoutParams(bottomParams);
//			actionbarBottomLine.setBackgroundColor(mSurveyContext.get()
//					.getColorObject().actionbar_underline);

			// back btn
			mBackBtn = new BackImageButton(context, mSurveyContext.get().getColorObject().icon);
			RelativeLayout.LayoutParams backParams = new RelativeLayout.LayoutParams(
					actionbarHeight, actionbarHeight);
			backParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			backParams.addRule(RelativeLayout.CENTER_VERTICAL);
			mBackBtn.setLayoutParams(backParams);
			mBackBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mSurveyContext.get().confirmExit();
				}
			});

			mTitleView = new AutoResizeTextView(context);
			RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			titleParams.leftMargin = actionbarHeight; // left
			titleParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			mTitleView.setLayoutParams(titleParams);
			mTitleView.setTextColor(mSurveyContext.get().getColorObject().actionbar_text);
			mTitleView.setTextSize(18);
			mTitleView.setText("User Voice");
			mTitleView.setSingleLine();
			mTitleView.setPadding(10, 0, 10, 0);
			mTitleView.setGravity(Gravity.CENTER);

//			actionbarArea.addView(actionbarBottomLine);
			actionbarArea.addView(mBackBtn);
			actionbarArea.addView(mTitleView);
			actionbarArea.setId(ACTIONBAR_ID);
			addView(actionbarArea);
		}
		// progressbar
		{
			mProgressBar = new ProgressBar(context, null,
					android.R.style.Widget_ProgressBar_Horizontal);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					Utils.getPxFromDip(context, 4));
			params.addRule(RelativeLayout.BELOW, ACTIONBAR_ID);
//			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//			params.setMargins(10, 10, 10, 10);
			mProgressBar.setLayoutParams(params);
			mProgressBar.setId(PROGRESS_ID);
			mProgressBar.setMax(10);
			ShapeDrawable fgDrawable = new ShapeDrawable(new RectShape());
			fgDrawable.getPaint().setColor(
					mSurveyContext.get().getColorObject().progress_active);
			ClipDrawable fgProgress = new ClipDrawable(fgDrawable,
					Gravity.LEFT, ClipDrawable.HORIZONTAL);
			mProgressBar.setProgressDrawable(fgProgress);
			Utils.setBackgroundDrawable(mProgressBar, new ColorDrawable(
					mSurveyContext.get().getColorObject().progress_inactive));
			mProgressBar.setProgress(3);
			addView(mProgressBar);
		}
		// btn area
		{
			int areaHeight = Utils.getPxFromDip(context, 44);

			RelativeLayout btnArea = new RelativeLayout(context);
			RelativeLayout.LayoutParams baseParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT, areaHeight);
			baseParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			baseParams.setMargins(10, 10, 10, 10);
//			baseParams.addRule(RelativeLayout.ABOVE, PROGRESS_ID);
			btnArea.setLayoutParams(baseParams);

			btnArea.setBackgroundColor(Color.TRANSPARENT);
			btnArea.setId(BTN_AREA);

			// prev btn
			mPrevBtn = new Button(context);
			RelativeLayout.LayoutParams leftParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			leftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			leftParams.leftMargin = Utils.getPxFromDip(context, 5);

			mPrevBtn.setLayoutParams(leftParams);
			mPrevBtn.setText(UserCafeString
					.getString(UserCafeString.prev_question));
			Utils.setBackgroundDrawable(mPrevBtn, new RectDrawable(mSurveyContext
					.get().getColorObject().next_button_bg));
			mPrevBtn.setTextColor(mSurveyContext.get().getColorObject().next_button_text);
			mPrevBtn.setVisibility(View.INVISIBLE);
			mPrevBtn.setTextSize(16);
			btnArea.addView(mPrevBtn);
			mPrevBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mPager.setCurrentItem(mLastQuestion.mRealPos - 1, true);
				}
			});

			// next btn
			mNextBtn = new Button(context);
			mNextBtn.setText(UserCafeString
					.getString(UserCafeString.next_question));
			Utils.setBackgroundDrawable(mNextBtn, new RectDrawable(mSurveyContext
					.get().getColorObject().next_button_bg));
			mNextBtn.setTextColor(mSurveyContext.get().getColorObject().next_button_text);
			RelativeLayout.LayoutParams rightParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			rightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//			rightParams.rightMargin = Utils.getPxFromDip(context, 5);
			mNextBtn.setLayoutParams(rightParams);
			mNextBtn.setTextSize(16);
			btnArea.addView(mNextBtn);
			mNextBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mLastQuestion.mRequired
							&& mLastQuestion.getAnswer() == null)
						RequiredAnswerIsNotFilled();
					else
						mPager.setCurrentItem(mLastQuestion.mRealPos + 1, true);
				}
			});

			addView(btnArea);
		}
		// content
		{
			contentArea = new LinearLayout(context);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			params.addRule(RelativeLayout.BELOW, PROGRESS_ID);
			params.addRule(RelativeLayout.ABOVE, BTN_AREA);
			contentArea.setLayoutParams(params);
			contentArea.setOrientation(LinearLayout.VERTICAL);

			int unit = Utils.getPxFromDip(context, 10);
			LinearLayout.LayoutParams layerParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			layerParams.gravity = Gravity.CENTER_VERTICAL;
			mFirstLayer = new TextView(context);
			mFirstLayer.setVisibility(View.GONE);
			mFirstLayer.setLayoutParams(layerParams);
			mFirstLayer.setBackgroundColor(mSurveyContext.get().getColorObject().first_layer_bg);
			mFirstLayer.setTextColor(mSurveyContext.get().getColorObject().first_layer_text);
			mFirstLayer.setPadding(unit, unit, unit, unit);
			mFirstLayer.setText("XXXXX");
			mFirstLayer.setId(LEVEL1_ID);
			contentArea.addView(mFirstLayer);

			mSecondLayer = new TextView(context);
			mSecondLayer.setVisibility(View.GONE);
			mSecondLayer.setLayoutParams(layerParams);
			mSecondLayer.setBackgroundColor(mSurveyContext.get().getColorObject().second_layer_bg);
			mSecondLayer.setTextColor(mSurveyContext.get().getColorObject().second_layer_text);
			mSecondLayer.setPadding(2 * unit, unit, unit, unit);
			mSecondLayer.setText("XXXXX");
			mSecondLayer.setId(LEVEL2_ID);
			contentArea.addView(mSecondLayer);

			mThirdLayer = new TextView(context);
			mThirdLayer.setVisibility(View.GONE);
			mThirdLayer.setLayoutParams(layerParams);
			mThirdLayer.setBackgroundColor(mSurveyContext.get().getColorObject().third_layer_bg);
			mThirdLayer.setTextColor(mSurveyContext.get().getColorObject().third_layer_text);
			mThirdLayer.setPadding(3 * unit, unit, unit, unit);
			mThirdLayer.setText("XXXXX");
			mThirdLayer.setId(LEVEL3_ID);
			contentArea.addView(mThirdLayer);

			mPager = new ViewPager(context);
			LinearLayout.LayoutParams pagerParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			pagerParams.gravity = Gravity.CENTER;
			mPager.setLayoutParams(pagerParams);
			mPager.setId(CONTENT_ID);
			mPager.setOnPageChangeListener(this);
			contentArea.addView(mPager);

			addView(contentArea);
		}
	}

	public void setMaxProgress(int max) {
		mProgressBar.setProgress(0);
		mProgressBar.setMax(max);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	private void modifyKeyboardAndFocus(BaseQuestion bq) {
		InputMethodManager imm = (InputMethodManager) getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null) {
			if (!(bq instanceof TextQuestion)) {
				imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);
			} else {
				EditText edit = ((TextQuestion) bq).mAnswerView;
				if (edit != null) {
					edit.requestFocus();
					imm.showSoftInput(edit, 0, null);
				}
			}
		}
	}

	void hideSomePage(int from, int to) {
		int currIdx = mSurveyContext.get().getPageIndexByPageNumber(from);
		int nextIdx = mSurveyContext.get().getPageIndexByPageNumber(to);

		boolean isChange = false;
		if (to == -1) {
			// submit page, hide all page
			to = mSurveyContext.get().findMaxPageNum();
			int maxIdx = mSurveyContext.get().getPageIndexByPageNumber(to);

			for (int idx = currIdx + 1; idx <= maxIdx; idx++) {
				isChange |= mSurveyContext.get()
						.setHiddenToPageIndex(idx, true);
			}
		} else {
			for (int idx = nextIdx + 1; idx < nextIdx; idx++) {
				isChange |= mSurveyContext.get()
						.setHiddenToPageIndex(idx, true);
			}
			isChange |= mSurveyContext.get().setHiddenToPageIndex(nextIdx,
					false);
		}
		if (isChange) {
			mPager.getAdapter().notifyDataSetChanged();
		}
	}

	@Override
	public void onPageSelected(int arg0) {
		final BaseQuestion currentBQ = mSurveyContext.get().getQuestions(false)
				.get(arg0);
		if (arg0 == 0) {
			mPrevBtn.setVisibility(View.INVISIBLE);
		} else {
			mPrevBtn.setVisibility(View.VISIBLE);
		}
		if (currentBQ instanceof SubmitQuestion) {
			mNextBtn.setVisibility(View.INVISIBLE);
			mProgressBar.setProgress(mProgressBar.getMax());
		} else {
			mNextBtn.setVisibility(View.VISIBLE);
			mProgressBar.setProgress(currentBQ.mPos);
		}

		if (mLastQuestion == currentBQ) {
			return;
		}

		if (currentBQ.mLastQuestion) {
			hideSomePage(currentBQ.mOwner.mPageNum, currentBQ.mOwner.mNextPage);
		}

		if (mLastQuestion.mRealPos < currentBQ.mRealPos
				&& mLastQuestion.mRequired && mLastQuestion.getAnswer() == null) {
			RequiredAnswerIsNotFilled();
			return;
		}
		modifyLayerArea(currentBQ);
		modifyKeyboardAndFocus(currentBQ);

		mLastQuestion.mStopTime = System.currentTimeMillis();
		currentBQ.mStartTime = System.currentTimeMillis();

		mLastQuestion = currentBQ;
	}

	private void RequiredAnswerIsNotFilled() {
		new AlertDialog.Builder(getContext())
				.setTitle(
						UserCafeString.getString(UserCafeString.required_title))
				.setMessage(
						UserCafeString.getString(UserCafeString.required_msg))
				.setPositiveButton(
						UserCafeString.getString(UserCafeString.required_ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								mPager.setCurrentItem(mLastQuestion.mRealPos,
										false);
							}
						}).setCancelable(false).show();
	}

	private void changeStateIfNeed(boolean show, final View v) {
		v.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	private void modifyLayerArea(BaseQuestion bq) {
		boolean showFirst = false;
		boolean showSecond = false;
		boolean showThird = false;
		if (bq.mFirstLayer != null) {
			mFirstLayer.setText(bq.mFirstLayer);
			showFirst = true;
		}

		if (bq.mSecondLayer != null) {
			mSecondLayer.setText(bq.mSecondLayer);
			showSecond = true;
		}

		if (bq.mThirdLayer != null) {
			mThirdLayer.setText(bq.mThirdLayer);
			showThird = true;
		}

		changeStateIfNeed(showFirst, mFirstLayer);
		changeStateIfNeed(showSecond, mSecondLayer);
		changeStateIfNeed(showThird, mThirdLayer);
	}
}
