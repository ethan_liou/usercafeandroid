package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONException;
import org.json.JSONObject;

import com.usercafe.ui.RectDrawable;
import com.usercafe.utils.Utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

class TextQuestion extends BaseQuestion implements TextWatcher {
	EditText mAnswerView;

	TextQuestion(Context context, WeakReference<SurveyContext> surveyContext,
			JSONObject json_obj) throws JSONException {
		super(context, surveyContext, json_obj);
	}

	@Override
	View fillContentView(ViewGroup container) {
		mAnswerView = new EditText(mContext);
		mAnswerView.setEms(10);
		mAnswerView.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		mAnswerView.clearFocus();
		mAnswerView.addTextChangedListener(this);
		Utils.setBackgroundDrawable(mAnswerView, new RectDrawable(
				mSurveyContext.get().getColorObject().button_bg));
		mAnswerView.setTextColor(mSurveyContext.get().getColorObject().button_text);
		container.addView(mAnswerView);
		return mAnswerView;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		setAnswer(s.length() == 0 ? null : s.toString());
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	protected boolean fillAnswer(ViewGroup container, String answer) {
		if (answer == null)
			return true;
		mAnswerView.setText(answer);
		return true;
	}
}
