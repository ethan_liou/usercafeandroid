package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import com.usercafe.resource.UserCafeImage;
import com.usercafe.resource.UserCafeString;
import com.usercafe.ui.RectDrawable;
import com.usercafe.utils.Base64;
import com.usercafe.utils.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

public class TimeQuestion extends BaseQuestion implements
		OnTimeChangedListener, OnClickListener, OnItemSelectedListener {
	enum TimeType {
		TIME_TYPE_NORMAL, TIME_TYPE_DURATION
	}

	final int TIME_ID = 1;
	final int HOUR_ID = 2;
	final int MIN_ID = 3;
	final int SEC_ID = 4;

	String mTimeString = null;
	TimeType mType;
	String mHour = null;
	String mMinute = null;
	String mSec = null;

	TimeQuestion(Context context, WeakReference<SurveyContext> surveyContext,
			JSONObject json_obj) {
		super(context, surveyContext, json_obj);
		mType = TimeType.values()[json_obj.optInt("time_type", 0)];
	}

	TimePicker generateTimePicker() {
		// time picker
		TimePicker picker = new TimePicker(mContext);
		Date initDate = new Date(System.currentTimeMillis());
		if (mTimeString != null) {
			try {
				DateFormat formatter = new SimpleDateFormat("HH:mm",
						Locale.getDefault());
				initDate = formatter.parse(mTimeString);
			} catch (ParseException e) {
			}
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(initDate);
		picker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
		picker.setCurrentMinute(calendar.get(Calendar.MINUTE));
		picker.setOnTimeChangedListener(this);
		return picker;
	}

	@Override
	View fillContentView(ViewGroup container) {
		if (mType == TimeType.TIME_TYPE_NORMAL) {
			Button timeBtn = new Button(mContext);
			Utils.setBackgroundDrawable(timeBtn, new RectDrawable(
					mSurveyContext.get().getColorObject().button_bg));
			timeBtn.setText(UserCafeString
					.getString(UserCafeString.clock_placeholder));
			timeBtn.setTextColor(mSurveyContext.get().getColorObject().button_text);
			timeBtn.setOnClickListener(this);
			timeBtn.setId(TIME_ID);
			byte[] clockImage = Base64.decode(UserCafeImage.CLOCK);
			int imageSize = Utils.getPxFromDip(mContext, 40);
			timeBtn.setCompoundDrawablesWithIntrinsicBounds(
					new BitmapDrawable(mContext.getResources(), Utils
							.scaleBitmap(clockImage, imageSize, imageSize)),
					null, null, null);
			container.addView(timeBtn);
			return timeBtn;
		} else {
			LinearLayout outLL = new LinearLayout(mContext);
			outLL.setOrientation(LinearLayout.VERTICAL);
			outLL.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			params.weight = 1;

			LinearLayout spinnerLL = new LinearLayout(mContext);
			spinnerLL.setOrientation(LinearLayout.HORIZONTAL);
			spinnerLL.setWeightSum(3);

			String[] objects = new String[61];
			for (int i = 0; i < objects.length; i++) {
				objects[i] = String.format("%02d", i - 1);
			}
			objects[0] = "--";

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_spinner_item, objects);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			Spinner hourSP = new Spinner(mContext);
			hourSP.setAdapter(adapter);
			hourSP.setLayoutParams(params);
			hourSP.setOnItemSelectedListener(this);
			hourSP.setId(HOUR_ID);
			spinnerLL.addView(hourSP);

			Spinner minuteSP = new Spinner(mContext);
			minuteSP.setAdapter(adapter);
			minuteSP.setLayoutParams(params);
			minuteSP.setOnItemSelectedListener(this);
			minuteSP.setId(MIN_ID);
			spinnerLL.addView(minuteSP);

			Spinner secondSP = new Spinner(mContext);
			secondSP.setAdapter(adapter);
			secondSP.setLayoutParams(params);
			secondSP.setOnItemSelectedListener(this);
			secondSP.setId(SEC_ID);
			spinnerLL.addView(secondSP);

			LinearLayout titleLL = new LinearLayout(mContext);
			titleLL.setOrientation(LinearLayout.HORIZONTAL);
			titleLL.setWeightSum(3);
			TextView hourTitle = new TextView(mContext);
			hourTitle.setText(UserCafeString.getString(UserCafeString.hour));
			hourTitle.setLayoutParams(params);
			titleLL.addView(hourTitle);

			TextView minuteTitle = new TextView(mContext);
			minuteTitle
					.setText(UserCafeString.getString(UserCafeString.minute));
			minuteTitle.setLayoutParams(params);
			titleLL.addView(minuteTitle);

			TextView secondTitle = new TextView(mContext);
			secondTitle.setText(UserCafeString.getString(UserCafeString.sec));
			secondTitle.setLayoutParams(params);
			titleLL.addView(secondTitle);

			outLL.addView(titleLL);
			outLL.addView(spinnerLL);
			container.addView(outLL);
			return outLL;
		}
	}

	@Override
	boolean fillAnswer(ViewGroup container, String answer) {
		if (answer != null && answer.length() != 0) {
			if (mType == TimeType.TIME_TYPE_NORMAL) {
				mTimeString = answer;
				Button btn = (Button) container.findViewById(TIME_ID);
				btn.setText(answer);
			} else {
				String[] durations = answer.split(":");
				mHour = durations[0];
				setSpinnerInitValue(container, HOUR_ID, mHour);
				mMinute = durations[1];
				setSpinnerInitValue(container, MIN_ID, mMinute);
				mSec = durations[2];
				setSpinnerInitValue(container, SEC_ID, mSec);
			}
		}
		return true;
	}

	private void setSpinnerInitValue(ViewGroup vg, int spinnerId, String str) {
		Spinner sp = (Spinner) vg.findViewById(spinnerId);
		sp.setSelection(Integer.parseInt(str) + 1);
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		mTimeString = "" + hourOfDay + ":" + minute;
	}

	@Override
	public void onClick(View v) {
		final Button btn = (Button) v;
		final TimePicker picker = generateTimePicker();
		new AlertDialog.Builder(mContext)
				.setTitle(
						UserCafeString
								.getString(UserCafeString.clock_placeholder))
				.setView(picker)
				.setPositiveButton(UserCafeString.getString(UserCafeString.ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								onTimeChanged(picker, picker.getCurrentHour(),
										picker.getCurrentMinute());
								btn.setText(mTimeString);
								setAnswer(mTimeString);
							}
						})
				.setNegativeButton(
						UserCafeString.getString(UserCafeString.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {

							}
						}).show();
	}

	void setString(int spinnerId, String str) {
		switch (spinnerId) {
		case HOUR_ID:
			mHour = str;
			break;
		case MIN_ID:
			mMinute = str;
			break;
		case SEC_ID:
			mSec = str;
			break;
		}
		if (mHour != null && mMinute != null && mSec != null)
			setAnswer("" + mHour + ":" + mMinute + ":" + mSec);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg2 != 0)
			setString(arg0.getId(), "" + arg2);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

}
