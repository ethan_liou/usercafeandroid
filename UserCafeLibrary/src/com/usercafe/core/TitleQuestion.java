package com.usercafe.core;

import java.lang.ref.WeakReference;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

class TitleQuestion extends BaseQuestion {
	TitleQuestion(Context context, WeakReference<SurveyContext> surveyContext,JSONObject json_obj) throws JSONException {
		super(context,surveyContext,json_obj);
		mTitle = json_obj.optString("title", "");
		mDesc = json_obj.optString("desc", "");
		mName = "_startpage";
		setAnswer("");
	}

	@Override
	void modifyTitleAndDesc(TextView titleTV, TextView descTV) {
		titleTV.setGravity(Gravity.CENTER_HORIZONTAL);
		descTV.setGravity(Gravity.CENTER_HORIZONTAL);
	}

	@Override
	protected View fillContentView(ViewGroup container) {
		View v = new View(mContext);
		return v;
	}

	@Override
	protected boolean fillAnswer(ViewGroup container,String answer) {
		return true;
	}

}
