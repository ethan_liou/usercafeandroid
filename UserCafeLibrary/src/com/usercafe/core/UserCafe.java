package com.usercafe.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.json.JSONObject;

import com.usercafe.push.UserCafePushPlugin;
import com.usercafe.resource.UserCafeColor;
import com.usercafe.resource.UserCafeString;
import com.usercafe.utils.Base64;

import android.Manifest.permission;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;

public class UserCafe {
	private static String TAG = "UserCafe";
	private static UserCafe sharedInstance;
	private List<UserCafePlugin> mPlugins;
	private String mAppId;
	private Queue<SurveyContext> mSurveyContexts;

	private UserCafe() {
		mPlugins = new ArrayList<UserCafePlugin>();
		mAppId = null;
		mSurveyContexts = new ArrayBlockingQueue<SurveyContext>(5);
	}

	public static enum CacheMode {
		NO_CACHE, USER;
	}

	public static enum PluginType {
		PLUGIN_PUSH
	};

	public static interface OnUserCafeCallback {
		public void onLoadingFinish(View view);

		public void onReturn(UserCafeReturnCode code);
	}

	public static enum UserCafeReturnCode {
		OK(-1), USER_STOP(0), INVALID_KEY(1), NETWORK_ERROR(2), SERVER_ERROR(3), ALREADY_FILLED(
				4);
		private int value;

		private UserCafeReturnCode(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}

	public static UserCafe sharedInstance() {
		if (sharedInstance == null)
			sharedInstance = new UserCafe();
		return sharedInstance;
	}

	public boolean isInit() {
		return mAppId != null;
	}

	private static boolean checkPermission(Context context) {
		// permission
		if (context.checkCallingOrSelfPermission(permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			Log.e(TAG, "You should check " + permission.READ_PHONE_STATE
					+ " is granted");
			return false;
		} else if (context.checkCallingOrSelfPermission(permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
			Log.e(TAG, "You should check " + permission.INTERNET
					+ " is granted");
			return false;
		}

		PackageManager pm = context.getPackageManager();
		try {
			PackageInfo pkg_info = pm.getPackageInfo(context.getPackageName(),
					PackageManager.GET_ACTIVITIES);
			ActivityInfo[] infos = pkg_info.activities;
			for (ActivityInfo info : infos) {
				if (info.name.endsWith("."
						+ SurveyActivity.class.getSimpleName()))
					return true;
			}
			Log.e(TAG,
					"You should add com.usercafe.core.SurveyActivity in AndroidManifest.xml");
			return false;
		} catch (NameNotFoundException e) {
			// pass
		}
		return true;
	}

	private static String oldCompatible(String key) {
		if (key.length() > 24) {
			String jsonString = "{'12Us2ge6KdAkmXh7gu3HMaOsZDd0xaCFy3377rK8EruY':'52134b71d06500e76763ba22','132VP315tXhnmHWvIaO-BrR2RP9IhozNAnMrAX-DCmiY':'520120efd06500e76763b9f9','1CbBhsp47R4_H9Ibr1JVEFdu_66xKN0WMhQr8i0-vT6E':'52135d40d06500e76763ba23','1KnlxAABFLDoQk08oFuEZNrEtr784TU7usNhP-eTZj00':'52156f10d06500e76763ba2f','1KrfnZf5HISW1-nQOz6MT34TgRBw40v5Sy2eG2rr-bFg':'52011fc3d06500e76763b9f8','1OAbTGAGpoZGldW4fWs0Djxr2LnyJ2Gf4PgBlgn_km5I':'52011a2cd06500e76763b9ed','1QoqUCwMN9mhXH-QKHUjRFoVmHaNcn52dHlDduGAFRC4':'52011a3ad06500e76763b9ef','1RIquXHgQvh7NoYx-MHf1v-1xeYOSR9vMIBbfand4EdU':'52010436d06500e76763b9e4','1S-BCKIU5ald3zaxN-eHgxekQBmSiZZ5GCto3cRP4vtE':'5215c8e5d06500e76763ba33','1Ur4mab_9wToGsqIN-OseWniO1zM3eZoqmXXbSPt8hjU':'52011a2bd06500e76763b9ec','1_2Ct4gixSDv104ksTTpy1vArDPv_GPmR1_rBHwuhRds':'52012377d06500e76763b9fb','1bY134JSBj33dq3OIwsN0w1Qu7VTSJrss3DxGPAymimw':'5215c8a3d06500e76763ba32','1m48-9OIjpfrNdVMfs5KOpXM5oFM2TqXtaiAv_u0nDWo':'52012430d06500e76763b9fd','1nOu1KlrQ8uQHwptAZZWGXQl2g-Tg8BJPNVJmKawXJP8':'5215c904d06500e76763ba34','1tcRttHXUogiZTRbILSUIuPGN7hC19DrrbJEry2hSSQA':'52011a2cd06500e76763b9ee','1w59mTQDJ-DQ6IIVm_o_uxEvnyZ4vLlmhM5yjXRSlRrA':'52012448d06500e76763b9fe','1zL-zIdQ1FkeslHfEbXzzYTrjq-Y1TberEnKk4br950I':'5215c925d06500e76763ba35','dC1TWmJ5eXZYLXFJUWw1SHppV0I3X0E6MQ':'52011a1cd06500e76763b9e9','dDAwMTdZazlkbE9EeHNBSlBicUtueEE6MQ':'52012260d06500e76763b9fa','dDhQaXRUeGNvY0RuTGl3MS1jZEYzVGc6MQ':'5215c849d06500e76763ba31','dE5DMkJtLVdwUENRbjJEUzB0ckpXcmc6MQ':'52011fb7d06500e76763b9f6','dEVUZXFEWlJHMGZZYkV3XzRjdXpTcWc6MQ':'52011a1bd06500e76763b9e8','dEVuT2xLS0ZZeXMwSEdoc3hLTjhtVnc6MQ':'52011a1dd06500e76763b9ea','dEY4NmljSnU3WndUa25meTl6cjU4RUE6MQ':'52011214d06500e76763b9e6','dFNqM1R3N2s4U3pfSHFGQWFQYUE5amc6MQ':'5215c819d06500e76763ba30','dFU2a3FoR001TkxPM3RSMlpEMWRtb0E6MQ':'52011dded06500e76763b9f4','dGIwSGUwRTVFMGJ6RHFSVFpmZ1NWMFE6MQ':'52011a2ad06500e76763b9eb','dGdTMzZEUVhkUXEyNkVjaEp2cC12Mmc6MQ':'52011e96d06500e76763b9f5','dHZ0Y004Tkg1VVNiMm9HTXpDQWRzTEE6MQ':'520116e6d06500e76763b9e7'}";
			try {
				JSONObject obj = new JSONObject(jsonString);
				byte[] decodedString = Base64.decode(key);
				if (decodedString != null) {
					String jsonStr = new String(decodedString);
					JSONObject json = new JSONObject(jsonStr);
					String form_key = json.getString("form_key");
					return obj.getString(form_key);
				}
			} catch (Exception e) {
			}
		}
		return key;
	}

	public Intent getIntent(Context context, String surveyId) {
		return UserCafe.getStaticIntent(context, mAppId, surveyId, "",
				CacheMode.USER, null, null);
	}

	public Intent getIntent(Context context, String surveyId,
			String submitDesc, CacheMode cacheMode,
			HashMap<String, String> customData, UserCafeColor customColor) {
		return UserCafe.getStaticIntent(context, mAppId, surveyId, submitDesc,
				cacheMode, customData, customColor);
	}

	public static Intent getStaticIntent(Context context, String appId,
			String surveyId, String submitDesc, CacheMode cacheMode,
			HashMap<String, String> customData, UserCafeColor customColor) {
		if (!checkPermission(context)) {
			return null;
		}

		String compatibleKey = oldCompatible(surveyId);
		UserCafeStateObject stateObj = new UserCafeStateObject(compatibleKey,
				appId, submitDesc, null, cacheMode.ordinal() - 2, customData,
				customColor);
		Intent intent = new Intent(context, SurveyActivity.class);
		intent.putExtra("stateObj", stateObj);
		return intent;
	}

	void openSurvey(Context context, String surveyId,
			OnUserCafeCallback listener) {
		if (listener == null) {
		}
		openSurvey(context, surveyId, null, CacheMode.NO_CACHE, null, null,
				listener);
	}

	SurveyContext openSurvey(Context context, String surveyId,
			String submitDesc, CacheMode cacheMode,
			HashMap<String, String> customData, UserCafeColor customColor,
			OnUserCafeCallback listener) {
		UserCafeStateObject stateObj = new UserCafeStateObject(surveyId,
				mAppId, submitDesc, null, cacheMode.ordinal() - 2, customData,
				customColor);
		SurveyContext surveyContext = new SurveyContext(context, stateObj,
				new WeakReference<UserCafe>(this), listener);
		surveyContext.loadSurveyFromServer();
		mSurveyContexts.add(surveyContext);
		return surveyContext;
	}

	String getAppId() {
		return mAppId;
	}

	void closeSurvey(SurveyContext surveyContext) {
		mSurveyContexts.remove(surveyContext);
	}

	public void AppCreate(final Context context, String appId,
			PluginType[] types) {
		mAppId = appId;
		// locale init
		UserCafeString.SetLanguage(context.getResources().getConfiguration().locale);

		// db init
		try {
			SQLiteDatabase db = DatabaseHelper.sharedDBHelper(context)
					.getReadableDatabase();
			db.close();
		} catch (Exception e) {
			// pass
			e.printStackTrace();
		}

		if (mPlugins == null)
			mPlugins = new ArrayList<UserCafePlugin>();
		else
			mPlugins.clear();
		if (types != null)
			for (PluginType type : types) {
				switch (type) {
				case PLUGIN_PUSH: {
					UserCafePushPlugin plugin = new UserCafePushPlugin();
					plugin.Init(context, appId);
					mPlugins.add(plugin);
					break;
				}
				default:
					break;
				}
			}

		// init action
		new Thread(new Runnable() {
			@Override
			public void run() {
				PendingAnswer.sendAllPendingAnswer(context);
			}
		}).start();
	}

	public void AppDestroy() {
		for (UserCafePlugin plugin : mPlugins) {
			plugin.DeInit();
		}
	}
}
