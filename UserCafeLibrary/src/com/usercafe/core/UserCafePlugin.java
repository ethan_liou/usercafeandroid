package com.usercafe.core;

import android.content.Context;

public interface UserCafePlugin {
	public void Init(Context context, String appId);
	public void DeInit();
}
