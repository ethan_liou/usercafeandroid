package com.usercafe.core;

import java.util.HashMap;

import com.usercafe.resource.UserCafeColor;

import android.os.Parcel;
import android.os.Parcelable;

public class UserCafeStateObject implements Parcelable {
	String mSurveyId;
	String mAppId;
	String mSubmitDesc;
	String mJSONString;
	int mCacheMode;
	HashMap<String, String> mCustomData;
	UserCafeColor mCustomColor;

	public UserCafeStateObject(String mSurveyId, String mAppId,
			String mSubmitDesc, String mJSONString, int mCacheMode,
			HashMap<String, String> mCustomData,UserCafeColor mCustomColor) {
		super();
		
		this.mSurveyId = mSurveyId;
		this.mAppId = mAppId;
		this.mSubmitDesc = mSubmitDesc;
		this.mJSONString = mJSONString;
		this.mCacheMode = mCacheMode;
		this.mCustomData = mCustomData;
		this.mCustomColor = mCustomColor == null ? new UserCafeColor() : mCustomColor;
	}

	@SuppressWarnings("unchecked")
	UserCafeStateObject(Parcel p) {
		mSurveyId = p.readString();
		mAppId = p.readString();
		mSubmitDesc = p.readString();
		mJSONString = p.readString();
		mCacheMode = p.readInt();
		mCustomData = (HashMap<String, String>) p.readSerializable();
		mCustomColor = (UserCafeColor) p.readParcelable(UserCafeColor.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mSurveyId);
		dest.writeString(mAppId);
		dest.writeString(mSubmitDesc);
		dest.writeString(mJSONString);
		dest.writeInt(mCacheMode);
		dest.writeSerializable(mCustomData);
		dest.writeParcelable(mCustomColor, flags);
	}

	public static final Parcelable.Creator<UserCafeStateObject> CREATOR = new Parcelable.Creator<UserCafeStateObject>() {
		@Override
		public UserCafeStateObject createFromParcel(Parcel source) {
			return new UserCafeStateObject(source);
		}

		@Override
		public UserCafeStateObject[] newArray(int size) {
			return new UserCafeStateObject[size];
		}
	};
}
