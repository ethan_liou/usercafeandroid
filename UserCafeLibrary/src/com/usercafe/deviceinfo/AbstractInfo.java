package com.usercafe.deviceinfo;

import android.content.Context;

public abstract class AbstractInfo {
	public AbstractInfo(Context context){
		mContext = context;
		mReady = false;
		mValue = null;
	}
	public abstract String getName();
	public abstract void loadValue();
	public String getValue(){
		return mReady ? mValue : "Unknown";
	}
	Context mContext;
	boolean mReady;
	String mValue;
}
