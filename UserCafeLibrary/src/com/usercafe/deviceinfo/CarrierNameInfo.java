package com.usercafe.deviceinfo;

import android.content.Context;
import android.telephony.TelephonyManager;

public class CarrierNameInfo extends AbstractInfo {
	public CarrierNameInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "carrierName";
	}

	@Override
	public
	void loadValue() {
		try {
			TelephonyManager tm = (TelephonyManager) mContext
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm != null) {
				String simName = tm.getSimOperatorName();
				if (simName == null || simName.length() == 0)
					simName = tm.getNetworkOperatorName();
				if (simName != null && simName.length() > 0){
					mValue = simName;
					mReady = true;
				}
			}

		} catch (Exception e) {
		}
	}
}
