package com.usercafe.deviceinfo;

import android.content.Context;
import android.telephony.TelephonyManager;

public class CarrierNumberInfo extends AbstractInfo {
	public CarrierNumberInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "carrierNumber";
	}

	@Override
	public
	void loadValue() {
		try {
			TelephonyManager tm = (TelephonyManager) mContext
					.getSystemService(Context.TELEPHONY_SERVICE);
			String num = null;
			if (tm != null) {
				num = tm.getSimOperator();
				if (num == null || num.length() == 0)
					num = tm.getNetworkOperator();
			}
			if (num != null) {
				mValue = num.substring(0, 3) + "," + num.substring(3);
				mReady = true;
			}
		} catch (Exception e) {
		}
	}
}
