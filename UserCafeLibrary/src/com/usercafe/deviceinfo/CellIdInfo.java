package com.usercafe.deviceinfo;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public class CellIdInfo extends AbstractInfo {
	public CellIdInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "cellId";
	}

	@Override
	public
	void loadValue() {
		try {
			TelephonyManager tm = (TelephonyManager) mContext
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm != null) {
				GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
				if (location != null){
					mValue = "" + location.getCid() + "," + location.getLac();
					mReady = true;
				}
			}
		} catch (Exception e) {
		}
	}
}
