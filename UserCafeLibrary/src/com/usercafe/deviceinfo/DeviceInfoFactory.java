package com.usercafe.deviceinfo;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

public class DeviceInfoFactory {
	public static List<AbstractInfo> generateInfos(Context context,
			String[] infoNames) {
		AbstractInfo candidates[] = { new CarrierNameInfo(context),
				new CarrierNumberInfo(context), new CellIdInfo(context),
				new DeviceModelInfo(context), new LocationInfo(context),
				new OSVersionInfo(context), new WifiBSSIDInfo(context) };
		List<AbstractInfo> infos = new ArrayList<AbstractInfo>(infoNames.length);
		for (String infoName : infoNames) {
			for (AbstractInfo candidate : candidates) {
				if (infoName.equalsIgnoreCase(candidate.getName())) {
					infos.add(candidate);
					break;
				}
			}
		}
		return infos;
	}
}
