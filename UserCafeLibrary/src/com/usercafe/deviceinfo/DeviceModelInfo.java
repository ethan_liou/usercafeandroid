package com.usercafe.deviceinfo;

import android.content.Context;
import android.os.Build;

public class DeviceModelInfo extends AbstractInfo {
	public DeviceModelInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "deviceModel";
	}

	@Override
	public
	void loadValue() {
		try {
			mValue = Build.MODEL;
			mReady = true;
		} catch (Exception e) {

		}
	}
}
