package com.usercafe.deviceinfo;

import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationInfo extends AbstractInfo {
	public LocationInfo(Context context) {
		super(context);
	}

	private void GetLocation() {
		try {
			final LocationManager lm = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);
			List<String> providers = lm.getProviders(true);
			for (String provider : providers) {
				lm.requestLocationUpdates(provider, 0, 0,
						new LocationListener() {
							@Override
							public void onStatusChanged(String provider,
									int status, Bundle extras) {
							}

							@Override
							public void onProviderEnabled(String provider) {
							}

							@Override
							public void onProviderDisabled(String provider) {
							}

							@Override
							public void onLocationChanged(Location location) {
								if (mValue == null) {
									mValue = "" + location.getLatitude()
											+ "," + location.getLongitude();
									mReady = true;
								}
								lm.removeUpdates(this);
							}
						});
			}
		} catch (Exception e) {

		}
	}

	private boolean GetLastLocation() {
		try {
			LocationManager lm = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);
			List<String> providers = lm.getProviders(true);
			for (String provider : providers) {
				Location location = lm.getLastKnownLocation(provider);
				if (location != null) {
					mValue = "" + location.getLatitude() + ","
							+ location.getLongitude();
					mReady = true;
					return true;
				}
			}
		} catch (Exception e) {
		}
		return false;
	}
	
	@Override
	public
	String getName() {
		return "location";
	}

	@Override
	public
	void loadValue() {
		GetLocation();
	}

	@Override
	public String getValue() {
		if(!mReady)
			GetLastLocation();
		return super.getValue();
	}
}
