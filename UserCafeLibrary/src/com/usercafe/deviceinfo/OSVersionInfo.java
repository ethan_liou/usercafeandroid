package com.usercafe.deviceinfo;

import android.content.Context;
import android.os.Build;

public class OSVersionInfo extends AbstractInfo {
	public OSVersionInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "osVersion";
	}

	@Override
	public
	void loadValue() {
		try {
			mValue = Build.VERSION.RELEASE;
			mReady = true;
		} catch (Exception e) {

		}
	}
}
