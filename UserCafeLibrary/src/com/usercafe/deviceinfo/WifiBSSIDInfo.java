package com.usercafe.deviceinfo;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class WifiBSSIDInfo extends AbstractInfo {
	public WifiBSSIDInfo(Context context) {
		super(context);
	}

	@Override
	public
	String getName() {
		return "wifiBSSID";
	}

	@Override
	public
	void loadValue() {
		try {
			WifiManager wm = (WifiManager) mContext
					.getSystemService(Context.WIFI_SERVICE);
			if (wm != null
					&& wm.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
				WifiInfo info = wm.getConnectionInfo();
				if (info != null) {
					mValue = info.getBSSID();
					mReady = true;
				}
			}
		} catch (Exception e) {
		}
	}
}
