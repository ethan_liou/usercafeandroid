package com.usercafe.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.usercafe.core.UserCafe;
import com.usercafe.core.UserCafe.CacheMode;

public abstract class UserCafePushNotification {
	private static final int NOTIFICATION_ID = 1;

	public static void addPendingNotification(Context context, Bundle bundle) {
		SharedPreferences.Editor editor = context.getSharedPreferences(
				UserCafePushNotification.class.getSimpleName(),
				Context.MODE_PRIVATE).edit();
		editor.clear();
		for (String key : bundle.keySet()) {
			editor.putString(key, bundle.getString(key));
		}
		editor.commit();
	}

	public static void sendNotification(Context context) {
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		SharedPreferences sp = context.getSharedPreferences(
				UserCafePushNotification.class.getSimpleName(),
				Context.MODE_PRIVATE);
		if (sp.contains("survey_id") && sp.contains("app_id")
				&& sp.contains("title")) {
			String title = sp.getString("title", "");
			Intent intent = UserCafe.getStaticIntent(context,
					sp.getString("app_id", ""), sp.getString("survey_id", ""),
					sp.getString("submit", ""), CacheMode.NO_CACHE, null,null);
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
					intent, 0);

			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					context)
					.setSmallIcon(android.R.drawable.ic_dialog_alert)
					.setContentTitle(title)
					.setStyle(
							new NotificationCompat.BigTextStyle()
									.bigText(title)).setContentText(title);

			builder.setContentIntent(contentIntent);

			Notification notification = builder.build();
			notification.flags = Notification.FLAG_AUTO_CANCEL;

			notificationManager.notify(NOTIFICATION_ID, notification);
		}
		sp.edit().clear().commit();
	}
}
