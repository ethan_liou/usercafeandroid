package com.usercafe.push;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.usercafe.core.Constant;
import com.usercafe.core.PredefinedCustomData;
import com.usercafe.core.UserCafePlugin;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

public class UserCafePushPlugin implements UserCafePlugin{
	private final String SENDER_ID = "881232093220";
	private final String PROPERTY_REG_ID = "registration_id";
	private final String PROPERTY_APP_VERSION = "appVersion";
	private final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTimeMs";
	private final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;
	private final String TAG = "UserCafePush";
	private String appId;

	public void Init(Context context, String app_id) {
		appId = app_id;
		String regid = getRegistrationId(context);

		if (regid.length() == 0) {
			registerBackground(context);
		}
		
		// notification
		UserCafePushNotification.sendNotification(context);
	}

	private int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.length() == 0) {
			Log.v(TAG, "Registration not found.");
			return "";
		}
		// check if app was updated; if so, it must clear registration id to
		// avoid a race condition if GCM sends a message
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion
				|| isRegistrationExpired(context)) {
			Log.v(TAG, "App version changed or registration expired.");
			return "";
		}
		return registrationId;
	}

	private SharedPreferences getGCMPreferences(Context context) {
		return context.getSharedPreferences(
				UserCafePlugin.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	private boolean isRegistrationExpired(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		// checks if the information is not stale
		long expirationTime = prefs.getLong(PROPERTY_ON_SERVER_EXPIRATION_TIME,
				-1);
		return System.currentTimeMillis() > expirationTime;
	}

	private void registerBackground(final Context context) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... _) {
				try {
					if (appId != null) {
						GoogleCloudMessaging gcm = GoogleCloudMessaging
								.getInstance(context);
						String regid = gcm.register(SENDER_ID);
						if (regid != null && regid.length() != 0) {
							// send to usercafe backend
							HttpClient client = new DefaultHttpClient();
							String url = String
									.format("%s/pushapi/app/%s/device/%s/register",
											Constant.SERVER_URL,appId, PredefinedCustomData.deviceId(context));
							HttpPost post = new HttpPost(url);
							ArrayList<NameValuePair> params = new ArrayList<NameValuePair>(
									2);
							params.add(new BasicNameValuePair("device_os", "0"));
							params.add(new BasicNameValuePair("token", regid));

							UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
									params, HTTP.UTF_8);
							post.setEntity(entity);
							HttpResponse response = client.execute(post);
							if (response.getStatusLine().getStatusCode() == 200)
								setRegistrationId(context, regid);
						}
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				return null;
			}
		}.execute(null, null, null);
	}

	private void setRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.v(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		long expirationTime = System.currentTimeMillis()
				+ REGISTRATION_EXPIRY_TIME_MS;

		Log.v(TAG, "Setting registration expiry time to "
				+ new Timestamp(expirationTime));
		editor.putLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
		editor.commit();
	}

	@Override
	public void DeInit() {
		// TODO Auto-generated method stub
		
	}
}
