package com.usercafe.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UserCafePushReceiver extends BroadcastReceiver {
	final int TRIGGER_NOW = 0;
	final int TRIGGER_NEXT_OPEN = 1;

	@Override
	public void onReceive(Context context, Intent intent) {
		String triggerString = intent.getStringExtra("trigger_method");
		int triggerMethod = triggerString == null ? TRIGGER_NOW : Integer.parseInt(triggerString);
		UserCafePushNotification.addPendingNotification(context, intent.getExtras());
		if(triggerMethod == TRIGGER_NOW)
			UserCafePushNotification.sendNotification(context);
	}
}
