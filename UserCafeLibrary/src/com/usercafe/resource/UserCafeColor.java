package com.usercafe.resource;

import android.os.Parcel;
import android.os.Parcelable;

public class UserCafeColor implements Parcelable{
	public int bg = 0xffe4e4e4;
	public int text = 0xff474a50;
	public int icon = 0xff198d7c;
	public int actionbar_bg = 0xffffffff;
	public int actionbar_text = 0xff474a50;
	public int actionbar_underline = 0xffececec;
	public int progress_inactive = 0xffc4d7d4;
	public int progress_active = 0xff198d7c;
	public int first_layer_bg = 0xffc8c8c8;
	public int first_layer_text = 0xff9d9d9d;
	public int second_layer_bg = 0xffd3d3d3;
	public int second_layer_text = 0xff838383;
	public int third_layer_bg = 0xffdadada;
	public int third_layer_text = 0xff646464;
	public int next_button_text = 0xffffffff;
	public int next_button_bg = 0xff76b1a7;
	public int scale_inactive_text = 0xff474a50;
	public int scale_inactive_bg = 0xffe0e0e0;
	public int scale_active_text = 0xff474a50;
	public int scale_active_bg = 0xff61847d;
	public int button_bg = 0xfff0f0f0;
	public int button_text = 0xff474a50;
	
	public UserCafeColor(){
		super();
	}
	
	UserCafeColor(Parcel p) {
		super();
		bg = p.readInt();
		text = p.readInt();
		icon = p.readInt();
		actionbar_bg = p.readInt();
		actionbar_text = p.readInt();
		actionbar_underline = p.readInt();
		progress_inactive = p.readInt();
		progress_active = p.readInt();
		first_layer_bg = p.readInt();
		first_layer_text = p.readInt();
		second_layer_bg = p.readInt();
		second_layer_text = p.readInt();
		third_layer_bg = p.readInt();
		third_layer_text = p.readInt();
		next_button_text = p.readInt();
		next_button_bg = p.readInt();
		scale_inactive_text = p.readInt();
		scale_inactive_bg = p.readInt();
		scale_active_text = p.readInt();
		scale_active_bg = p.readInt();
		button_bg = p.readInt();
		button_text = p.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(bg);
		dest.writeInt(text);
		dest.writeInt(icon);
		dest.writeInt(actionbar_bg);
		dest.writeInt(actionbar_text);
		dest.writeInt(actionbar_underline);
		dest.writeInt(progress_inactive);
		dest.writeInt(progress_active);
		dest.writeInt(first_layer_bg);
		dest.writeInt(first_layer_text);
		dest.writeInt(second_layer_bg);
		dest.writeInt(second_layer_text);
		dest.writeInt(third_layer_bg);
		dest.writeInt(third_layer_text);
		dest.writeInt(next_button_text);
		dest.writeInt(next_button_bg);
		dest.writeInt(scale_inactive_text);
		dest.writeInt(scale_inactive_bg);
		dest.writeInt(scale_active_text);
		dest.writeInt(scale_active_bg);
		dest.writeInt(button_bg);
		dest.writeInt(button_text);
	}

	public static final Parcelable.Creator<UserCafeColor> CREATOR = new Parcelable.Creator<UserCafeColor>() {
		@Override
		public UserCafeColor createFromParcel(Parcel source) {
			return new UserCafeColor(source);
		}

		@Override
		public UserCafeColor[] newArray(int size) {
			return new UserCafeColor[size];
		}
	};
}
