
package com.usercafe.resource;

import java.util.Locale;

public abstract class UserCafeString {
	private static String[] SUPPORT_LOCALE = { "en","zh_TW","ko","ja","zh_HK","zh_CN","ar","th","es","id","it","pt_PT","pt_BR","ru","ms","de","fr","tr","vi" };
	public static int language = 0;
	
	public static final String[] edittext_hint  = { "Input","在這裡輸入文字","답변을 입력하세요","ここに文字を入力してください","在這裡輸入文字","在这里输入文字","ادخل النص هنا","ป้อนข้อมูล","Escriba aquí ","Ketik di sini","","Entrada ","Entrada","Написать","","Geben Sie Wörter hier ein","Saisir ici","","Nhập chữ ở đây" };
	public static final String[] next_question = { "Next","下一題","다음 ","次へ","下一題","下一题","السؤال التالي","ต่อไป","siguiente","Berikutnya","","Próximo ","Próximo","Следующий вопрос","","nächste Frage","Suivant","","Câu hỏi tiếp theo" };
	public static final String[] prev_question = { "Prev","上一題","이전 ","前へ","上一題","上一题","السؤال السابق","ก่อนหน้า","anterior ","Sebelumnya","","Anterior ","Anterior","Прошлый вопрос","","vorige Frage","Précédent","","Câu hỏi trước" };
	public static final String[] required_title = { "Notice","小提醒","안내","通知","小提醒","小提醒","تذكير","คำเตือน","aviso","Peringatan","","Aviso ","Aviso","Уведомление","","Achtung","Rappel","","Nhắc nhỏ" };
	public static final String[] date_placeholder = { "Select date","選擇日期","날짜 선택","日付を選択","選擇日期","选择日期","حدد التاريخ","เลือกวันที่","seleccione fecha","Pilih tanggal","","Seleccionar data ","Seleccionar data","Выбрать дату","","wählen Datum","Sélectionner la date","","Chọn ngày" };
	public static final String[] clock_placeholder = { "Select time","選擇時間","시간을 선택","時間を選択","選擇時間","选择时间","حدد الوقت","เลือกเวลา","seleccione horas","Pilih waktu","","Selecione o tempo ","Selecione o tempo","Выбрать время","","wählen Zeit","Sélectionner l'heure","","Chọn thời gian" };
	public static final String[] required_msg = { "This question is required!","此題為必答喔！","답변을 선택하셔야만 다음 문항으로 넘어가실 수 있습니다","この質問を必ず答えてください！","此題為必答喔！","此题为必答喔！","سؤال يمكن تجاهلة !","ต้องตอบคำถามนี้ก่อน","¡Es una pregunta requerida!","Pertanyaan ini harus dijawab!","","Esta questão é necessária! ","Esta pergunta é necessária!","Этот вопрос необходимо ответить","","Sie müssen auf diese Frage antworten!","Question obligatoire","","Câu hỏi này bắt buộc trả lời" };
	public static final String[] required_ok = { "Got it!","知道了","확인","かしこまりました","知道了","知道了","حصلت علية","รู้แล้ว","Ya veo.","Sudah tahu","","Entendi! ","Entendi!","Понятно","","verstanden","Oui","","Đã biết" };
	public static final String[] other_hint = { "Others","其他","기타","その他","其他","其他","أخر","อื่นๆ","otros","Lainnya","","Outros ","Outros","Другой","","sonstiges","Autres","","Khác" };
	public static final String[] loading  = { "Processing","處理中","처리중입니다","処理中です","處理中","处理中","تحويل","กำลังดำเนินการ","procesando ","Sedang diproses","","Processamento ","Processamento","Загрузка","","verarbeitend","En cours de traitement","","Đang xử lý" };
	public static final String[] please_wait = { "Please wait","請稍等","잠시만 기다려 주세요","少々お待ちしてください","請稍等","请稍等","برجاء الانتظار","กรุณารอสักครู่","Espere por favor. ","Harap tunggu","","Espere por favor ","Espere por favor","Подождите","","Moment Bitte ","Veuillez patienter.","","Hãy đợi" };
	public static final String[] submit = { "Submit","提交問卷","제출하기","提出する","提交問卷","提交问卷","عرض","นำเสนอ","entregar","Kumpulkan","","Submeter ","Submeter","представлять","","Legen Sie den Fragebogen vor","Soumettre","","Nộp bảng câu hỏi" };
	public static final String[] send = { "Send","送出","제출","送信","送出","送出","إرسال","ส่งออก","enviar","Kirim","","Envie","Mandar","Отправлять","","senden","Envoyer","","Gửi đi" };
	public static final String[] back_title = { "Are you sure to exit","你要離開問卷了嗎","이 페이지에서 나가시겠습니까?","移動しますか？","你要離開問卷了嗎","你要离开问卷了吗","تأكيد الانهاء","คุณต้องการออกจากเพจนี้หรือไม่","¿Está seguro de que desea salir?","Apakah Anda ingin keluar","","Tem certeza que deseja sair ","Tem certeza que deseja sair ","Выходите из анкеты","","Verlassen Sie dieser Fragebogen?","Êtes-vous sûr de quitter?","","Bạn muốn thoát khỏi bảng câu hỏi phải không" };
	public static final String[] back_ok = { "Exit","離開","종료","終了","離開","离开","إنهاء","ออก","salir","Keluar","","Saída ","Saída ","Выход","","Verlassen","Quitter","","Thoát" };
	public static final String[] cancel = { "Cancel","取消","취소","キャンセル","取消","取消","إلغاء","ยกเลิก","cancelar","Batal","","Cancelar ","Cancelar ","Отменить","","löschen","Annuler","","Hủy" };
	public static final String[] ok = { "OK","確定","확인","確認","確定","确定","نعم","ตกลง","OK","OK","","OK","OK","хорошо","","OK","Confirmer","","Xác định" };
	public static final String[] network_title = { "Network issue","無法連線","인터넷에 연결할 수 없습니다.","インターネットに接続できません。","無法連線","无法连线","غير قادر على الاتصال","ไม่สามารถเชื่อมต่อได้","problemas de la red ","Tidak dapat disambung","","Problema de rede ","Problema de rede ","не подключить ","","Keine Netzverbindung ","Connexion échouée","","Không thể kết nối" };
	public static final String[] network_msg  = { "Please check your network settings, and send it again!","請檢查網路設定再試一次","네트워크 설정을 확인하고 다시 보내 주시기 바랍니다.","ネットワークの設定を確認して再度送信してください。","請檢查網路設定再試一次","请检查网路设定再试一次","يرجى التحقق من الشبكة","กรุณาตรวจสอบการตั้งค่าอินเตอร์เน็ต","Por favor verifique la configuración de red y volver a enviarlo.","Harap periksa sambungan internet lalu dicoba sekali lagi","","Por favor, verifique as configurações de rede, e enviá-lo novamente! ","Por favor, verifique as configurações de rede, e enviá-lo novamente! ","Пожалуйста проверьте настройки вашего сетевого подключения.выслайте еще раз","","Bitte prüfen Sie Ihre Internet Situation und versuchen Sie noch einmal.","Veuillez vérifier la connexion internet.","","Hãy kiểm tra cài đặt mạng lần nữa" };
	public static final String[] sec  = { "secs","秒","초","秒","秒","秒","ثانية","วินาที","sec.","Detik","","Seg ","Seg ","секунда","","Sekunde","seconde","","Giây" };
	public static final String[] minute = { "mins","分","분","分","分","分","دقيقة","นาที","min.","Menit","","Minutos ","Minutos ","минут","","Minute","minute","","Phút" };
	public static final String[] hour = { "hours","小時","시간","時間","小時","小时","ساعة","ชั่วโมง","horas","Jam","","Horas ","Horas ","час","","Stunde","heure","","Tiếng" };
	public static final String[] several_day = { "several days","幾天","몇일","数日","幾天","几天","عدة ايام","หลายวัน","varios días ","Beberapa hari","","Vários dias","Vários dias","Несколько дней","","ein paar Tage","quelques jours","","Mấy ngày" };
	public static final String[] unfinish_msg = { "Do you want to complete your survey you fill %s ago?","%s前，你有一份未完成的問卷你要繼續把他完成嗎？","%s전에 작성하신 설문을 계속 진행하시겠습니까?","%s前に完成されていたアンケートを続行しますか？","%s前，你有一份未完成的問卷你要繼續把他完成嗎？","%s前，你有一份未完成的问卷你要继续把他完成吗？","لم تكمل الاستبيان يجب أن تستمر لأكمل ما قبل s%","คุณต้องการกรอกใบสำรวจต่อจากเมื่อ%sวันก่อนหรือไม่","%s Quiere completar el cuestionario en el que ha terminado ?","Anda masih memiliki pertanyaan yang belum dijawab di depan%s, apakah Anda ingin menyelesaikannya?","","Antes de preencher%s, tu quer completar a sua pesquisa?","Antes de preencher%s, você quer completar a sua pesquisa?","Do you want to complete your survey you fill %s ago?","","Bevor %s, Sie haben noch einen Fragebogen nicht ausgefüllt, wollen Sie weiter machen? ","Voulez-vous continuer le questionnaire que vous avez commencé il y a %s ?","","Trước %s , bạn có một bảng câu hỏi chưa trả lời, Bạn muốn tiếp tục hoàn thành nó không?" };

	public static String getDiffTimeStr(long diffMSec) {
		int diffSec = (int) (diffMSec / 1000);
		if (diffSec < 60) {
			return "" + diffSec + getString(UserCafeString.sec);
		} else if (diffSec < 60 * 60) {
			return "" + diffSec / 60 + getString(UserCafeString.minute);
		} else if (diffSec < 24 * 60 * 60) {
			return "" + diffSec / 60 / 60 + getString(UserCafeString.hour);
		} else {
			return getString(UserCafeString.several_day);
		}
	}

	public static String getString(String[] string) {
		return string[language];
	}

	public static void SetLanguage(Locale locale) {
		language = 0;
		try {
			for (int i = 0; i < SUPPORT_LOCALE.length; i++) {
				String[] tmpLng = SUPPORT_LOCALE[i].split("-");
				if (tmpLng.length == 2) {
					// lng-country
					if (locale.getLanguage().equalsIgnoreCase(tmpLng[0])
							&& locale.getCountry().equalsIgnoreCase(tmpLng[1])) {
						language = i;
						break;
					}
				} else if (tmpLng.length == 1) {
					if (locale.getLanguage().equalsIgnoreCase(tmpLng[0])) {
						language = i;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

