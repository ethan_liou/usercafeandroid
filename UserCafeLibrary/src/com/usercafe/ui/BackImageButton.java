package com.usercafe.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.widget.ImageButton;

public class BackImageButton extends ImageButton {
	int mColor;
	Paint mPaint;
	Path mPath;
	
	public BackImageButton(Context context) {
		super(context);
		throw(new UnsupportedOperationException());
	}
	
	public BackImageButton(Context context,int color) {
		super(context);
		mColor = color;
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStyle(Style.STROKE);
		mPaint.setColor(color);
		setBackgroundColor(Color.TRANSPARENT);
		mPath = new Path();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int width = getWidth();
		int height = getHeight();
		int length = (int) ((width < height ? width : height)/1.5f);
		int left = width/2-length/2;
		int right = left + length;
		int top = height/2 - length/2;
		int bottom = top + length;
		mPaint.setStrokeWidth(length/10);
		mPath.moveTo((left+right)/2, top + length/10);
		mPath.lineTo(left+length/6, (top+bottom)/2);
		mPath.lineTo((left+right)/2, bottom - length/10);
		canvas.drawPath(mPath, mPaint);
	}
}
