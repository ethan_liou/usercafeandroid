package com.usercafe.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.widget.CheckBox;

public class CustomCheckBox extends CheckBox {
	int mActiveColor;
	Paint mOuterPaint;
	Paint mInnerPaint;	
	
	Rect mDrawRect;
	Path mPath;
	
	public CustomCheckBox(Context context) {
		super(context);
		throw(new UnsupportedOperationException());
	}
	
	public CustomCheckBox(Context context,int activeColor) {
		super(context);
		mActiveColor = activeColor;
		mOuterPaint = new Paint();
		mOuterPaint.setAntiAlias(true);
		mOuterPaint.setStrokeWidth(2f);
		mOuterPaint.setStyle(Style.STROKE);
		mOuterPaint.setColor(Color.LTGRAY);
		mInnerPaint = new Paint();
		mInnerPaint.setAntiAlias(true);
		mInnerPaint.setColor(activeColor);
		mInnerPaint.setStyle(Style.STROKE);
		
		mDrawRect = new Rect();
		mPath = new Path();
	}
	
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		int width = getWidth();
		int height = getHeight();
		int length = (int) ((width < height ? width : height)/1.5f);
		int left = width/2-length/2;
		int right = left + length;
		int top = height/2 - length/2;
		int bottom = top + length;
		mDrawRect.set(left, top, right, bottom);
		canvas.drawRect(mDrawRect, mOuterPaint);
		if(isChecked()){
			mInnerPaint.setStrokeWidth(length/4);
			mPath.moveTo(left+length/10, (top+bottom)/2);
			mPath.lineTo((top+bottom)/2, bottom-length/10);
			mPath.lineTo(right, top);
			canvas.drawPath(mPath, mInnerPaint);
		}
	}
}
