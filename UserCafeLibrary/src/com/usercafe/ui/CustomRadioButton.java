package com.usercafe.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.widget.RadioButton;

public class CustomRadioButton extends RadioButton {
	int mActiveColor;
	Paint mOuterPaint;
	Paint mInnerPaint;	
	public CustomRadioButton(Context context) {
		super(context);
		throw(new UnsupportedOperationException());
	}
	
	public CustomRadioButton(Context context,int activeColor) {
		super(context);
		mActiveColor = activeColor;
		mOuterPaint = new Paint();
		mOuterPaint.setAntiAlias(true);
		mOuterPaint.setStrokeWidth(1f);
		mOuterPaint.setStyle(Style.STROKE);
		mOuterPaint.setColor(Color.DKGRAY);
		mInnerPaint = new Paint();
		mInnerPaint.setAntiAlias(true);
		mInnerPaint.setStyle(Style.FILL);
	}
	
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		int width = getWidth();
		int height = getHeight();
		canvas.drawCircle(width/2, height/2, (width < height ? width : height)/3.0f, mOuterPaint);
		mInnerPaint.setColor(isChecked() ? mActiveColor : Color.LTGRAY);
		canvas.drawCircle(width/2, height/2, (width < height ? width : height)/5.0f, mInnerPaint);
	}
}
