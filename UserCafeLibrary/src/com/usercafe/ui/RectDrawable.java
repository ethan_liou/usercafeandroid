package com.usercafe.ui;

import android.graphics.drawable.PaintDrawable;

public class RectDrawable extends PaintDrawable {
    public RectDrawable(int color) {
        super(color);
        setCornerRadius(5f);
    }
}
