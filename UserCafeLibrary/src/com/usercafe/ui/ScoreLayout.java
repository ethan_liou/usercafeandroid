package com.usercafe.ui;

import com.usercafe.utils.Utils;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ScoreLayout extends LinearLayout {
	private GridView mGridView;
	private TextView mLeftTextView;
	private TextView mRightTextView;
	private int mMaxScore = 5;

	public ScoreLayout(Context context, int maxScore, int textColor){
		this(context);
		mMaxScore = maxScore;
		if(mLeftTextView != null){
			mLeftTextView.setTextColor(textColor);
		}
		if(mRightTextView != null){
			mRightTextView.setTextColor(textColor);
		}
	}
	
	public ScoreLayout(Context context) {
		super(context);
		setOrientation(VERTICAL);
		LinearLayout textArea = new LinearLayout(context);
		textArea.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		params.weight = 1;

		mLeftTextView = new TextView(context);
		mLeftTextView.setLayoutParams(params);
		mLeftTextView.setGravity(Gravity.LEFT);

		textArea.addView(mLeftTextView);

		mRightTextView = new TextView(context);
		mRightTextView.setLayoutParams(params);
		mRightTextView.setGravity(Gravity.RIGHT);
		mRightTextView.setTextColor(Color.BLACK);

		textArea.addView(mRightTextView);
		addView(textArea);

		mGridView = new GridView(context);

		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		@SuppressWarnings("deprecation")
		int width = wm.getDefaultDisplay().getWidth() / 5;
		
		LinearLayout.LayoutParams gridParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, width * 2);
		mGridView.setLayoutParams(gridParams);
		mGridView.setGravity(Gravity.CENTER);
		mGridView.setHorizontalSpacing(Utils.getPxFromDip(context, 4));
		mGridView.setVerticalSpacing(Utils.getPxFromDip(context, 4));
		mGridView.setNumColumns(5);
		addView(mGridView);
	}

	public void setLeftText(String text){
		if(mLeftTextView != null)
			mLeftTextView.setText(text+"\n1");
	}
	
	public void setRightText(String text){
		if(mRightTextView != null)
			mRightTextView.setText(text+"\n"+mMaxScore);
	}
	
	public void setAdapter(BaseAdapter adapter){
		if(mGridView != null)
			mGridView.setAdapter(adapter);
	}
	
	public void setOnItemClickListener(OnItemClickListener listener){
		if(mGridView != null)
			mGridView.setOnItemClickListener(listener);
	}
}
