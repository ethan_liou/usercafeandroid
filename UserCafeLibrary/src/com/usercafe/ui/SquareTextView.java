package com.usercafe.ui;


import android.content.Context;
import android.widget.TextView;

public class SquareTextView extends TextView {
	public SquareTextView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}
}
