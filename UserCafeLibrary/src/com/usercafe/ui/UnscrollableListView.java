package com.usercafe.ui;

import com.usercafe.utils.Utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

public class UnscrollableListView extends LinearLayout {
	private BaseAdapter adapter;
	private OnItemClickListener onItemClickListener;

	public void notifyChange() {
		int count = getChildCount();
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		for (int i = count; i < adapter.getCount(); i++) {
			final int index = i;
			final LinearLayout layout = new LinearLayout(getContext());
			layout.setLayoutParams(params);
			layout.setOrientation(VERTICAL);
			View v = adapter.getView(i, null, null);
			v.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (onItemClickListener != null) {
						onItemClickListener.onItemClick(UnscrollableListView.this,layout, index, adapter.getItem(index));
					}
				}
			});

			View divider = new View(getContext());
			LayoutParams llParams = new LayoutParams(LayoutParams.MATCH_PARENT, 2);
			llParams.leftMargin = 10;
			llParams.rightMargin = 10;			
			divider.setLayoutParams(llParams);
			divider.setBackgroundColor(Color.parseColor("#dddddd"));
			layout.addView(v);
			if(i != adapter.getCount() - 1)
				layout.addView(divider);
			addView(layout, index);
		}
	}

	public UnscrollableListView(Context context){
		super(context);
		throw(new UnsupportedOperationException());
	}
	
	public UnscrollableListView(Context context, int color) {
		super(context);
		setOrientation(VERTICAL);
		Utils.setBackgroundDrawable(this, new RectDrawable(color));
	}

	public BaseAdapter getAdapter() {
		return adapter;
	}

	public void setAdapter(BaseAdapter adpater) {
		this.adapter = adpater;
		notifyChange();
	}

	public void setOnItemClickListener(OnItemClickListener onClickListener) {
		this.onItemClickListener = onClickListener;
	}

	public static interface OnItemClickListener {
		public void onItemClick(ViewGroup parent, View view, int position,
				Object o);
	}	
}
