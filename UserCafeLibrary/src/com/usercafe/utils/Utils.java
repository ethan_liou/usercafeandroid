package com.usercafe.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public abstract class Utils {
	public static HttpClient generateClient(){
		HttpParams params = new BasicHttpParams();
		int timeoutConnection = 15000;
		HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(params, timeoutSocket);
		return new DefaultHttpClient(params);
	}
	static String deviceId = null;
	public static Bitmap scaleBitmap(byte[] data,int width,int height){
		Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);
		Bitmap scaledBM = Bitmap.createScaledBitmap(bm, width, height, true);
		return scaledBM;
	}
	
	public static int getPxFromDip(Context context,int dip){
	    Resources r = context.getResources();
	    return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, r.getDisplayMetrics());
	}
	
	public static int getPxFromSp(Context context,int sp){
	    Resources r = context.getResources();
	    return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, r.getDisplayMetrics());
	}
	
	public static void setBackgroundDrawable(View v,Drawable d){
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			v.setBackgroundDrawable(d);
		} else {
			v.setBackground(d);
		}

	}
}
