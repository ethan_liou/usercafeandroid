package com.usercafe.demo;
import com.usercafe.resource.UserCafeColor;

public class CustomColor extends UserCafeColor{
	public CustomColor(){
		bg = 0xffe4e4e4;
		text = 0xff474a50;
		icon = 0xff198d7c;
		actionbar_bg = 0xffffff32;
		actionbar_text = 0xff474a50;
		actionbar_underline = 0xffececec;
		progress_inactive = 0xffc4d7d4;
		progress_active = 0xff198d7c;
		first_layer_bg = 0xffc8c8c8;
		first_layer_text = 0xff9d9d9d;
		second_layer_bg = 0xffd3d3d3;
		second_layer_text = 0xff838383;
		third_layer_bg = 0xffdadada;
		third_layer_text = 0xff646464;
		next_button_text = 0xffffffff;
		next_button_bg = 0xff76b1a7;
		scale_inactive_text = 0xff474a50;
		scale_inactive_bg = 0xffe0e0e0;
		scale_active_text = 0xff474a50;
		scale_active_bg = 0xff61847d;
		button_bg = 0xfff0f0f0;
		button_text = 0xff474a50;
	}
}