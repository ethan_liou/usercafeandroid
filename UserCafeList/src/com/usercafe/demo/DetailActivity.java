package com.usercafe.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.usercafe.core.UserCafe;
import com.usercafe.core.UserCafe.CacheMode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class DetailActivity extends Activity {
	String mKey;
	List<String> keys;

	void refreshUI(String json) {
		try {
			JSONArray arr = new JSONArray(json);
			List<Map<String, String>> data = new ArrayList<Map<String, String>>(
					arr.length());
			keys = new ArrayList<String>(arr.length());
			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				Map<String, String> d = new HashMap<String, String>(2);
				d.put("title", obj.optString("title", ""));
				data.add(d);
				keys.add(obj.optString("_id", ""));
			}
			ListView lv = new ListView(DetailActivity.this);
			lv.setAdapter(new SimpleAdapter(DetailActivity.this, data,
					android.R.layout.simple_list_item_1,
					new String[] { "title" }, new int[] { android.R.id.text1 }));
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					String key = keys.get(position);
					Intent intent = UserCafe.sharedInstance().getIntent(
							DetailActivity.this, key, "", CacheMode.USER, null,
							null);
					if (intent != null)
						startActivityForResult(intent, 0);
				}
			});
			LinearLayout layout = new LinearLayout(this);
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.addView(lv);
			setContentView(layout);

			return;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Toast.makeText(DetailActivity.this, "Unknown Error...",
				Toast.LENGTH_LONG).show();
	}

	void reload() {
		new AsyncTask<Void, Void, String>() {
			ProgressDialog pd;

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				pd = ProgressDialog.show(DetailActivity.this, "Loading...",
						"Please wait...");
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				pd.dismiss();
				if (result == null) {
					Toast.makeText(DetailActivity.this, "Network issue!",
							Toast.LENGTH_LONG).show();
				} else {
					refreshUI(result);
				}
			}

			@Override
			protected String doInBackground(Void... params) {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpGet post = new HttpGet(
							"http://usercafe.whoscall.com/api/private/app/" + mKey
									+ "/survey");
					HttpResponse response = client.execute(post);
					return EntityUtils.toString(response.getEntity());
				} catch (Exception e) {

				}
				return null;
			}
		}.execute();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String key = getIntent().getStringExtra("key");
		String title = getIntent().getStringExtra("title");
		if (title != null)
			setTitle(title);
		if (key != null) {
			UserCafe.sharedInstance().AppCreate(this, key, null);
			mKey = key;
			reload();
			View v = new View(this);
			setContentView(v);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Change Account");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final EditText et = new EditText(this);
		et.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		new AlertDialog.Builder(this).setTitle("Input Alias").setView(et)
				.setNegativeButton("Cancel", null)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent intent = UserCafe.sharedInstance().getIntent(
								DetailActivity.this, et.getText().toString(),
								"", CacheMode.USER, null, null);
						if (intent != null)
							startActivityForResult(intent, 0);
					}
				}).show();
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.e("Ret", "" + resultCode + "," + requestCode);
		Toast.makeText(this, "" + resultCode, Toast.LENGTH_LONG).show();
	}
}
