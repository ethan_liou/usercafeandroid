package com.usercafe.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MainActivity extends Activity {
	String DEFAULT_ACCOUNT = "usercafe.share@gmail.com";
	String account;
	List<String> keys;

	void refreshUI(String json) {
		try {
			JSONArray arr = new JSONArray(json);
			final List<Map<String, String>> data = new ArrayList<Map<String, String>>(
					arr.length());
			keys = new ArrayList<String>(arr.length());
			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				Map<String, String> d = new HashMap<String, String>(2);
				d.put("title", obj.optString("name", ""));
				data.add(d);
				keys.add(obj.optString("_id", ""));
			}
			ListView lv = new ListView(MainActivity.this);
			lv.setAdapter(new SimpleAdapter(MainActivity.this, data,
					android.R.layout.simple_list_item_1,
					new String[] { "title" }, new int[] { android.R.id.text1 }));
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Intent intent = new Intent(MainActivity.this,DetailActivity.class);
					intent.putExtra("key", keys.get(position));
					intent.putExtra("title", data.get(position).get("title"));
					startActivityForResult(intent,0);
				}
			});
			// RelativeLayout rl = new RelativeLayout(this);

			LinearLayout layout = new LinearLayout(this);
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.addView(lv);
			setContentView(layout);

			return;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Toast.makeText(MainActivity.this, "Unknown Error...", Toast.LENGTH_LONG)
				.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.e("OnAct",""+resultCode);
	}

	void reload() {
		new AsyncTask<Void, Void, String>() {
			ProgressDialog pd;

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				pd = ProgressDialog.show(MainActivity.this, "Loading...",
						"Please wait...");
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				pd.dismiss();
				if (result == null) {
					Toast.makeText(MainActivity.this, "Network issue!",
							Toast.LENGTH_LONG).show();
				} else {
					refreshUI(result);
				}
			}

			@Override
			protected String doInBackground(Void... params) {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpGet post = new HttpGet(
							"http://usercafe.whoscall.com/api/private/user/"+account+"/app");
					HttpResponse response = client.execute(post);
					return EntityUtils.toString(response.getEntity());
				} catch (Exception e) {

				}
				return null;
			}
		}.execute();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		account = getSharedPreferences("account", MODE_PRIVATE).getString(
				"account", DEFAULT_ACCOUNT);
		reload();
		View v = new View(this);
		setContentView(v);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final EditText et = new EditText(this);
		et.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		et.setText(account);
		new AlertDialog.Builder(this).setTitle("Input UserCafe Account")
				.setView(et).setNegativeButton("Cancel", null)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						account = et.getText().toString();
						getSharedPreferences("account", MODE_PRIVATE).edit()
								.putString("account", account).commit();
						reload();
					}
				}).show();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Change Account");
		return super.onCreateOptionsMenu(menu);
	}
}
