package com.usercafe.pushdemo;

import com.usercafe.core.UserCafe;

import android.os.Bundle;
import android.app.Activity;

public class UserCafeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_cafe);
		UserCafe.sharedInstance().AppCreate(this, "5205faac2a4f563f90c4f5d4",
				new UserCafe.PluginType[] { UserCafe.PluginType.PLUGIN_PUSH });
	}
}
